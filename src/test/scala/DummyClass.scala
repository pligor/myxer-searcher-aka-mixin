/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class ParentClass {
  def justdoit() {
    println("inside parent class");
  }
}

class DummyClass extends ParentClass with TraitLeft with TraitRight {
  override def justdoit() {
    super.justdoit();
    println("inside dummy class");
  }
}

trait TraitLeft extends ParentClass {
  override def justdoit() {
    super.justdoit();
    println("inside trait left");
  }
}

trait TraitRight extends ParentClass {
  override def justdoit() {
    super.justdoit();
    println("inside trait right");
  }
}
