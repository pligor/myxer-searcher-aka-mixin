import org.specs2.mutable.Specification

//import play.api.test._
//import Helpers._

/**
 * Created by pligor
 */
class DummySpec extends Specification {
  sequential;
  "The correct execution of DummySpec" should {
    "mean that Specs2 is working" in {

      val dummy = new DummyClass;

      dummy.justdoit();

      success;
    }
  }
}



