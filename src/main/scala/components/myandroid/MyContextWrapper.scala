package components.myandroid

import android.content.{ContextWrapper, BroadcastReceiver}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyContextWrapper extends ContextWrapper {
  protected implicit val context = this;

  protected implicit lazy val resources = getResources;

  def safeUnregisterReceiver(receiver: BroadcastReceiver) {
    try {
      unregisterReceiver(receiver);
    }
    catch {
      case e: IllegalArgumentException => {
        //do nothing for the time being
      }
    }
  }
}
