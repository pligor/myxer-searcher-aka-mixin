package components.myandroid

import android.app.Activity

import com.bugsense.trace.BugSenseHandler
import android.os.Bundle
import components.helpers.log
import components.helpers.AndroidHelper._

object BugsenseActivity {
  private val API_KEY = "2539adf7";
  val WEBSITE = "http://bugsense.com"
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * TRY USING THIS TRAIT AS "RIGHT" AS POSSIBLE because if it is left the right traits will take precedence
 */
trait BugsenseActivity extends Activity {
  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    if (isDebuggable(this)) {
      log log "we do NOT enable bugsense on debuggable version of the apk";
    } else {
      BugSenseHandler.initAndStartSession(this, BugsenseActivity.API_KEY);
      log log "on bugsense initialization";
    }
  }

  /*override def onDestroy() {
    super.onDestroy();
    BugSenseHandler.closeSession(this);
  }*/
}
