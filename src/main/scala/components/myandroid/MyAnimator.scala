package components.myandroid

import android.view.animation._
import android.view.animation.Animation.AnimationListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyAnimator {
  def fadeInAndOut(totalDuration: Int)(afterAnimation: => Unit): Animation = {
    val step = totalDuration/2;

    val fadeIn = new AlphaAnimation(0, 1);
    fadeIn.setInterpolator(new DecelerateInterpolator());
    fadeIn.setDuration(step);

    val fadeOut = new AlphaAnimation(1, 0);
    fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
    fadeOut.setStartOffset(step);
    fadeOut.setDuration(step);

    val animation = new AnimationSet(false); //change to false
    animation.addAnimation(fadeIn);
    animation.addAnimation(fadeOut);

    animation.setAnimationListener(new AnimationListener {
      def onAnimationEnd(animation: Animation) {
        afterAnimation;
      }

      def onAnimationStart(animation: Animation) {}

      def onAnimationRepeat(animation: Animation) {}
    })

    animation;
  }
}
