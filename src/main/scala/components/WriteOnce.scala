package components

class WriteOnceException extends Exception;

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class WriteOnce[T] {
  private var value: Option[T] = None;

  def apply() = value.getOrElse(throw new UninitializedError);

  def isInitialized = value.isDefined;

  def setValue(x: T) {
    if (isInitialized) {
      throw new WriteOnceException;
    }
    else {
      value = Some(x);
    }
  }
}
