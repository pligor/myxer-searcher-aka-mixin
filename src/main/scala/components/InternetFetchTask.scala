package components

import scala.io.Source

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

class InternetFetchTask(op: (Option[String]) => Unit) extends ScalaAsyncTask[String, AnyRef, Option[String]] {
  protected def doInBackground(url: String): Option[String] = {
    try {
      Some(Source.fromURL(url).getLines().mkString(niou line));
    } catch {
      case e: Exception => None;
    }
  }

  override def onPostExecute(result: Option[String]) {
    super.onPostExecute(result);
    op(result);
  }
}