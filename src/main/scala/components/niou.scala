package components

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object niou {
  /**
   * @return platform independent newline
   */
  def line: String = System.getProperty("line.separator");
}
