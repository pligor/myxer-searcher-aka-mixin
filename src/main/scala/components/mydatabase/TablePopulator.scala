package components.mydatabase

import android.content.{Context, ContentValues}
import models.{FavoriteItem, MyxerItemModel}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private object TablePopulator {
  def apply(insert: (String, ContentValues) => Long)(implicit context: Context) {
    //DATABASE OPERATIONS

    //Models
    /*val front1 = new Photo("front1." + Photo.FILENAME_EXT, pgpKeyId).insert;
    val back1 = new Photo("back1." + Photo.FILENAME_EXT, pgpKeyId).insert;*/

    val item1 = new MyxerItemModel();
    val itemId1 = item1.insert;
    assert(itemId1 != -1);

    val item2 = new MyxerItemModel();
    item2.setArtist("hello there");
    item2.setPrice(0.55F);
    val itemId2 = item2.insert;
    assert(itemId2 != -1);

    val favoriteItem1 = new FavoriteItem(rating = 0.45F, ordernum = 0, itemId = Some(itemId2));
    val favItemId1 = favoriteItem1.insert;
    assert(favItemId1 != -1);
  }
}
