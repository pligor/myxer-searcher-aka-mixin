package components.mydatabase

import android.content.{Context, ContentValues}
import android.database.Cursor
import android.content
import java.lang
import models.FavoriteItem

//import net.sqlcipher.database.{SQLiteOpenHelper, SQLiteDatabase}

import android.database.sqlite.{SQLiteDatabase, SQLiteOpenHelper}
import android.util.Log
import collection.mutable.ListBuffer
import components.helpers.log
import com.pligor.myxer_searcher.MyxerSearcher._

//with this sql you can get information/metadata for tables: pragma table_info(tablename in here)

//TODO use this to populate, init database??: https://github.com/jgilfelt/android-sqlite-asset-helper

//TODO use the two links below to make the database encrypted
//https://guardianproject.info/code/sqlcipher/
//https://github.com/sqlcipher/android-database-sqlcipher
object DatabaseHandler {
  val invalidAutoIncrementId = 0;

  val DATABASE_NAME = "myxersearcher";

  val defaultIdCol = "id";

  val seperator = ", ";

  val cursorBaseIndex = 0;

  val invalidInsertId = -1;

  def genBinders(len: Int) = ("?," * len).dropRight(1);
}

/**
 * We create this object for the specific database, NOT for each table
 */
class DatabaseHandler(implicit val context: Context) extends SQLiteOpenHelper(
  context,
  DatabaseHandler.DATABASE_NAME,
  null,
  2
) {
  //in order to change between encrypted and plain database just change these two lines
  def grabReadableDatabase = getReadableDatabase /*(DB_PASS)*/ ;

  private def grabWritableDatabase = getWritableDatabase /*(DB_PASS)*/ ;

  def workWithReadableDatabase[T](op: (SQLiteDatabase) => T): T = {
    val db = grabReadableDatabase;
    try {
      op(db);
    } finally {
      db.close();
    }
  }

  def workWithWritableDatabase[T](op: (SQLiteDatabase) => T): T = {
    val db = grabWritableDatabase;
    try {
      op(db);
    } finally {
      db.close();
    }
  }

  lazy val DATABASE_VERSION = {
    val db = grabReadableDatabase;
    val version = try {
      db.getVersion;
    }
    finally {
      db.close();
    }
    version;
  }

  def reset(toPopulate: Boolean = false) {
    Log.i("pl", "before try deletion of db");
    val deleted: Boolean = context.deleteDatabase(DatabaseHandler.DATABASE_NAME);
    Log.i("pl", if (deleted) "deleted" else "not deleted");

    val db: SQLiteDatabase = grabReadableDatabase; // simply to trigger create methods if necessary ;)

    try {
      if (deleted && toPopulate) {
        populate();
      }
    }
    finally {
      db.close();
    }
  }

  private def populate() {
    TablePopulator(insert);
    log log ("finished populating");
  }

  def onCreate(db: SQLiteDatabase) {
    TableCreator(db);
    log log ("tables were created successfully");
  }

  /**
   * http://blog.adamsbros.org/2012/02/28/upgrade-android-sqlite-database/
   */
  def onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    log log "oldVersion: " + oldVersion;
    log log "newVersion: " + newVersion;

    var upgradeTo = oldVersion + 1;
    while (upgradeTo <= newVersion) {
      upgradeTo match {
        case 2 => {
          FavoriteItem.introduceOrdering(db);
        }
      }
      upgradeTo += 1;
    }
  }

  def getSingleScalar[A](query: String)(implicit m: Manifest[A]): Option[A] = {
    val db = grabReadableDatabase;
    try {
      val cursor = db.rawQuery(query, null);
      if (cursor.moveToFirst()) {
        Some(getCursorMethodByType[A](cursor).apply(DatabaseHandler.cursorBaseIndex).asInstanceOf[A]);
        //Some(cursor.getInt(DatabaseHandler.cursorBaseIndex));
      }
      else {
        None;
      }
    }
    finally {
      db.close();
    }
  }

  /**
   * sample usage: getCursorMethodByType[A](cursor).apply(DatabaseHandler.cursorBaseIndex).asInstanceOf[A]
   */
  private def getCursorMethodByType[A](cursor: Cursor)(implicit m: Manifest[A]): (Int) => Any = {
    //NOTE: for some reason we must first store the manifests in vals before doing pattern matching
    val arrayByteManifest = manifest[Array[Byte]];
    val doubleManifest = manifest[Double];
    val floatManifest = manifest[Float];
    val intManifest = manifest[Int];
    val longManifest = manifest[Long];
    val shortManifest = manifest[Short];
    val stringManifest = manifest[String];

    manifest[A] match {
      case `arrayByteManifest` => cursor.getBlob
      case `doubleManifest` => cursor.getDouble
      case `floatManifest` => cursor.getFloat
      case `intManifest` => cursor.getInt
      case `longManifest` => cursor.getLong
      case `shortManifest` => cursor.getShort
      case `stringManifest` => cursor.getString
      case _ => throw new Exception("this kind of type is not supported");
    }
  }

  def updateByCompoundId(tableName: String,
                         compoundId: Map[String, Long],
                         contentValues: ContentValues
                          ): Boolean = {
    val db: SQLiteDatabase = grabWritableDatabase;
    val rowsAffected = try {
      enableForeignKeys(db);

      val whereClause = compoundId.map(_._1 + " = ?").mkString(" AND ");

      val whereArgs = compoundId.map(_._2.toString).toArray;

      db.update(tableName, contentValues, whereClause, whereArgs);
    } finally {
      db.close();
    }

    rowsAffected == 1;
  }

  //TODO refactor updateById is just a special occasion of updateByCompoundId
  def updateById(tableName: String,
                 modelId: Long,
                 contentValues: ContentValues,
                 columnName: String): Boolean = {
    val db: SQLiteDatabase = grabWritableDatabase;

    val rowsAffected = try {
      enableForeignKeys(db);

      val whereClause = columnName + " = ?";

      val whereArgs = Array[String](modelId.toString);

      db.update(tableName, contentValues, whereClause, whereArgs);
    } finally {
      db.close();
    }
    //Log.i("pl", "updated model with id: " + modelId + " on table: " + tableName);
    rowsAffected == 1;
  }

  def deleteByCompoundId(tableName: String,
                         compoundId: Map[String, Long]): Boolean = {
    val db: SQLiteDatabase = grabWritableDatabase;

    val rows: Int = try {
      enableForeignKeys(db);

      val whereClause = compoundId.map(_._1 + " = ?").mkString(" AND ");

      val whereArgs = compoundId.map(_._2.toString).toArray;

      db.delete(tableName, whereClause, whereArgs);
    } finally {
      db.close();
    }

    rows == 1;
  }

  //TODO refactor deleteById is just a special occasion of deleteByCompoundId

  def deleteById(tableName: String,
                 modelId: Long,
                 columnName: String): Boolean = {
    val db: SQLiteDatabase = grabWritableDatabase;

    val rows: Int = try {
      enableForeignKeys(db);

      db.delete(tableName, columnName + " = ?", Array[String](modelId.toString));
    }
    finally {
      db.close();
    }
    //log log("deleted model with id: " + modelId + " on table: " + tableName);

    rows == 1;
  }

  def getModelByQuery[A <: MyModel](queryWITHargs: String, args: Array[String])(factory: (Cursor) => A): Option[A] = {
    val db = grabReadableDatabase;
    try {
      cursor2model(
        db.rawQuery(queryWITHargs, args)
      )(factory);
    }
    finally {
      db.close();
    }
  }

  def getModelByQuery[A <: MyModel](queryWithOUTargs: String)(factory: (Cursor) => A): Option[A] = {
    val db = grabReadableDatabase;

    try {
      cursor2model(
        db.rawQuery(queryWithOUTargs, null)
      )(factory);
    }
    finally {
      db.close();
    }
  }

  def getModelsByQuery[A](queryWITHargs: String, args: Array[String])(factory: (Cursor) => A): Seq[A] = {
    val db: SQLiteDatabase = grabReadableDatabase;

    try {
      val cursor = db.rawQuery(queryWITHargs, args);
      if (cursor.moveToFirst()) {
        val listBuffer = ListBuffer.empty[A];
        do {
          listBuffer += factory(cursor);
        } while (cursor.moveToNext());
        listBuffer.toSeq;
      }
      else {
        Seq.empty[A];
      }
    }
    finally {
      db.close();
    }
  }

  def getModelsByQuery[A](queryWithOUTargs: String)(factory: (Cursor) => A): Seq[A] = {
    val db: SQLiteDatabase = grabReadableDatabase;

    try {
      val cursor = db.rawQuery(queryWithOUTargs, null);
      if (cursor.moveToFirst()) {
        val listBuffer = ListBuffer.empty[A];
        do {
          listBuffer += factory(cursor);
        } while (cursor.moveToNext());
        listBuffer.toSeq;
      }
      else {
        Seq.empty[A];
      }
    }
    finally {
      db.close();
    }
  }

  def getNextAutoIncrementOption(tableName: String): Option[Long] = {
    val db: SQLiteDatabase = grabReadableDatabase;

    val query = "SELECT * FROM SQLITE_SEQUENCE WHERE name = ?";
    val selectionArgs = Array[String](tableName);

    try {
      val cursor = db.rawQuery(query, selectionArgs);
      if (cursor.moveToFirst()) {
        val nextAutoIncrement = cursor.getLong(cursor.getColumnIndexOrThrow("seq"))
        assert(!cursor.moveToNext(), "should have been only one row returned");
        Some(nextAutoIncrement);
      }
      else {
        None;
      }
    }
    finally {
      db.close();
    }
  }

  private def cursor2model[A <: MyModel](cursor: Cursor)(factory: (Cursor) => A): Option[A] = {
    if (cursor.moveToFirst()) {
      val optionA = Some(factory(cursor));
      assert(!cursor.moveToNext(), "should have been only one row returned");
      optionA;
    }
    else {
      None;
    }
  }

  /**
   * NOTE: only works for models with id column
   */
  def getById[A <: MyModel](tableName: String, modelId: Long, columnName: String)(factory: (Cursor) => A): Option[A] = {
    val db: SQLiteDatabase = grabReadableDatabase;

    try {
      // Cursor cursor = db.query(TABLE_CONTACTS, new String[] { // KEY_ID, // KEY_NAME, // KEY_PH_NO // }, KEY_ID + "=?", new String[] { //
      // String.valueOf(id)
      // }, null, null, null, null);
      val sql = "SELECT * FROM " + tableName + " WHERE " + columnName + " = ?";

      val selectionArgs = Array[String](modelId.toString);

      //val cursor: Cursor = db.rawQuery(sql, selectionArgs);
      /* if (cursor.moveToFirst()) {
         val optionA = Some(factory(cursor));
         assert(cursor.moveToNext() == false); //verify it was only one row returned
         optionA;
       }
       else {
         None;
       }*/

      cursor2model(
        db.rawQuery(sql, selectionArgs)
      )(factory);
    }
    finally {
      db.close();
    }
  }

  def getAll[A <: MyModel](tableName: String)(factory: (Cursor) => A): Seq[A] = {
    getModelsByQuery("SELECT * FROM " + tableName)(factory);
  }

  /**
   * REMEMBER TO CLOSE THE CURSOR AFTERWARDS
   */
  def getCursorAll[A <: MyModel](tableName: String): Cursor = {
    val query = "SELECT id AS _id, * FROM " + tableName;
    val db: SQLiteDatabase = grabReadableDatabase;
    db.rawQuery(query, null);
  }

  def insert(tableName: String, contentValues: ContentValues): Long = {
    val db: SQLiteDatabase = grabWritableDatabase;

    val newId = try {
      enableForeignKeys(db);
      db.insert(tableName, null, contentValues);
      //Log.i("pl", "inserted id: " + insertedId);
      //getAutoIncrement(db);
    }
    finally {
      db.close();
    }

    newId;
  }

  def getCount(tableName: String): Long = {
    val sql: String = "SELECT COUNT(*) FROM " + tableName;

    val db: SQLiteDatabase = grabReadableDatabase;

    try {
      val cursor: Cursor = db.rawQuery(sql, null);

      assert(cursor.moveToFirst());

      cursor.getInt(DatabaseHandler.cursorBaseIndex);
    } finally {
      db.close();
    }
  }

  /**
   * i THINK it works only with a writeable database as input
   */
  private def getAutoIncrement(db: SQLiteDatabase): Long = {
    val sql: String = "SELECT last_insert_rowid()";
    val cursor: Cursor = db.rawQuery(sql, null);

    if (cursor.moveToFirst()) {
      cursor.getInt(DatabaseHandler.cursorBaseIndex);
    }
    else {
      DatabaseHandler.invalidAutoIncrementId;
    }
  }

  /**
   * @param db closing the database must happen from outside
   */
  private def enableForeignKeys(db: SQLiteDatabase) {
    db.rawQuery("PRAGMA foreign_keys = 1", null);
  }
}
