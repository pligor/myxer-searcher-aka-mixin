package components.mydatabase

import android.database.Cursor
import android.content.{ContentValues, Context}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait MyModelObject {
  val TABLE_NAME: String;

  val ID_COLUMN_NAME = DatabaseHandler.defaultIdCol;

  def getCount(implicit context: Context) = {
    (new DatabaseHandler).getCount(TABLE_NAME);
  }

  //def createFromCursor[A <: MyModel](cursor: Cursor): A;
  def createFromCursor(cursor: Cursor): MyModel;

  //def getAll[A <: MyModel](implicit context: Context): Seq[A] = {
  def getAll(implicit context: Context): Seq[MyModel] = {
    (new DatabaseHandler).getAll(TABLE_NAME)(createFromCursor);
  }

  //TODO check if the solution in comments underneath works
  /*def createFromCursorLeme[A <: MyModel](cursor: Cursor): A;
  def getLeme[A <: MyModel](modelId: Int)(implicit context: Context): Option[A] = {
    (new DatabaseHandler).getById(TABLE_NAME, modelId)(createFromCursorLeme);
  }*/

  //def getById[Α <: MyModel](modelId: Int)(implicit context: Context): Option[Α] = {
  def getById(modelId: Long)(implicit context: Context): Option[MyModel] = {
    (new DatabaseHandler).getById(TABLE_NAME, modelId, columnName = ID_COLUMN_NAME)(createFromCursor);
  }

  def getCursorAll(implicit context: Context): Cursor = {
    (new DatabaseHandler).getCursorAll(TABLE_NAME);
  }
}

abstract class MyModel(val id: Option[Long]) {
  val TABLE_NAME: String;

  val ID_COLUMN_NAME: String;

  //TODO find out if this is possible to get from the metadata info of a table
  def isIdAutoIncrement: Boolean;

  def isOld = id.isDefined;

  def isNew = !isOld;

  protected def generateContentValues: ContentValues;

  def update(implicit context: Context): Boolean = {
    require(id.isDefined);
    (new DatabaseHandler).updateById(TABLE_NAME, id.get, generateContentValues, ID_COLUMN_NAME);
  }

  def delete(implicit context: Context) = {
    require(id.isDefined);
    (new DatabaseHandler).deleteById(TABLE_NAME, id.get, ID_COLUMN_NAME);
  }

  def insert(implicit context: Context) = {
    if (isIdAutoIncrement) {
      require(!id.isDefined);
    }
    (new DatabaseHandler).insert(TABLE_NAME, generateContentValues);
  }
}
