package components.mydatabase

import android.database.sqlite.SQLiteDatabase
//import net.sqlcipher.database.SQLiteDatabase

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private object TableCreator {
  def apply(db: SQLiteDatabase) {
    val createStatements = Seq[String](
      """CREATE TABLE myxeritemmodel (
        |    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    price REAL,
        |    created INTEGER,
        |    name TEXT,
        |    itemlink TEXT,
        |    itemsendlink TEXT,
        |    artist TEXT,
        |    profilelink TEXT,
        |    type TEXT,
        |    sendlink TEXT,
        |    preview TEXT,
        |    smallthumbnail TEXT,
        |    thumbnail TEXT,
        |    bigthumbnail TEXT
        |)""",

      """CREATE TABLE favoriteitem (
        |	itemId INTEGER PRIMARY KEY NOT NULL,
        |	rating REAL NOT NULL,
        | ordernum INT NOT NULL DEFAULT 0,
        | UNIQUE(ordernum)
        |	FOREIGN KEY(itemId) REFERENCES myxeritemmodel(id)
        |		ON UPDATE RESTRICT ON DELETE CASCADE
        |)"""
    );

    createStatements foreach {
      createStatement =>
        db.execSQL(createStatement.stripMargin);
    }
  }
}
