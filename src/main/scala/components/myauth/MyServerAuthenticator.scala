package components.myauth

import components.helpers.UrlHelper._
import scala.io.{Codec, Source}
import components.niou
import java.io.{InputStreamReader, ByteArrayInputStream}
import com.google.gson.stream.JsonReader
import components.helpers.log
import java.net.URL
import uk.co.bigbeeconsultants.http.HttpClient

sealed trait SituationException extends Exception;

protected object Situation extends Enumeration {
  val USERNAME_EXISTS_PASSWORD_WRONG = Value;
  val SIGNUP_SUCCESS = Value;
  val ACTIVATION_PENDING = Value;
  val SIGNIN_SUCCESS = Value;
}

protected case class MyServerResponse(situation: Situation.Value, token: Option[String]);

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyServerAuthenticator {
  private val charset = Codec.UTF8;

  /**
   * mAuthTokenType is the type of token that I request from the server.
   * I can have the server give me different tokens for read-only or full access to an account,
   * or even for different services within the same account.
   * A good example is the Google account, which provides several auth-token types:
   * “Manage your calendars”, “Manage your tasks”, “View your calendars” and more..
   * On this particular example I don’t do anything different for the various auth-token types.
   */
  //you should set the same string on the server
  val defaultAuthTokenType = "auth.token";

  /**
   * @return token empty string or null is considered an invalid token (see AccountAuthenticator)
   */
  def getin(userName: String, userPass: String, authTokenType: String): Option[MyServerResponse] = {
    //TODO we are ignoring authTokenType for the time being (useful if different users will have different permissions)

    //TODO we should do a POST request later and to be a Json request with json body [https://github.com/FaKod/sjersey-client]
    /*val result = Source.fromURL(createCheckinUrl(userName,userPass)).getLines().mkString(niou line);
    log log result;*/

    /*implicit val httpClient = new ApacheHttpClient
    val response = GET(createCheckinUrl(userName,userPass)).executeUnsafe;
    log log response.bodyString(charset);
    */

    val httpClient = new HttpClient;
    try {
      val response = httpClient.get(createCheckinUrl(userName, userPass));
      log log response.status.toString;
      val result = response.body.asString;
      log log result;

      val byteInputStream = new ByteArrayInputStream(result.getBytes(charset));
      val reader = new JsonReader(new InputStreamReader(byteInputStream, charset))
      val serverResponse = try {
        reader.beginObject();

        assert(reader.nextName() == "situation");
        val situation = Situation.withName(reader.nextString());
        val tokenOption = if (reader.hasNext) {
          assert(reader.nextName() == "token");
          val token = reader.nextString();
          Some(token);
        } else {
          None;
        }

        reader.endObject();

        MyServerResponse(situation, tokenOption);
      } finally {
        reader.close();
      }

      Some(serverResponse);
    } catch {
      case e: Exception => {
        None;
      }
    }
  }

  private def createCheckinUrl(username: String, password: String) = {
    val urlString = "http://myxersearcherserver.sunsheriffs.com/getin/" + urlEncode(username) + "/" + urlEncode(password);
    log log urlString;
    new URL(urlString);
  }
}
