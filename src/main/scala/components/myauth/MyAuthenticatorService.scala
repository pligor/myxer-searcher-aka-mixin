package components.myauth

import android.app.Service
import android.content.Intent
import android.os.IBinder
import components.myandroid.MyContextWrapper

object MyAuthenticatorService {
  private var accountAuthenticator: AccountAuthenticator = null;
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyAuthenticatorService extends Service with MyContextWrapper {
  def onBind(intent: Intent): IBinder = {
    if(intent.getAction == android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT) {
      getAuthenticator.getIBinder;
    } else {
      null;
    }
  }

  private def getAuthenticator = {
    if(MyAuthenticatorService.accountAuthenticator == null) {
      MyAuthenticatorService.accountAuthenticator = new AccountAuthenticator(context);
    }
    MyAuthenticatorService.accountAuthenticator;
  }
}
