package components.myauth

import android.accounts.{Account, AccountManager, AccountAuthenticatorResponse, AbstractAccountAuthenticator}
import android.content.{Intent, Context}
import android.os.Bundle
import android.text.TextUtils
import components.helpers.log

object AccountAuthenticator {
  val ACCOUNT_TYPE = "myxer.searcher.account.type";
  val EMPTY_TOKEN = "";
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * http://www.jiahaoliuliu.com/2012/05/android-account-manager-part-i.html
 * http://www.jiahaoliuliu.com/2012/06/android-account-manager-part-ii.html
 */
class AccountAuthenticator(val context: Context) extends AbstractAccountAuthenticator(context) {

  def editProperties(response: AccountAuthenticatorResponse, accountType: String): Bundle = {
    log log "editProperties";
    null
  }

  def confirmCredentials(p1: AccountAuthenticatorResponse, p2: Account, p3: Bundle): Bundle = {
    log log "confirmCredentials";
    null;
  }

  def getAuthTokenLabel(p1: String): String = "auth token label";

  def updateCredentials(p1: AccountAuthenticatorResponse, p2: Account, p3: String, p4: Bundle): Bundle = {
    log log "updateCredentials";
    null;
  }

  def hasFeatures(p1: AccountAuthenticatorResponse, p2: Account, p3: Array[String]): Bundle = {
    log log "hasFeatures";
    null;
  }

  def addAccount(response: AccountAuthenticatorResponse,
                 accountType: String,
                 authTokenType: String,
                 requiredFeatures: Array[String],
                 options: Bundle): Bundle = {
    val bundle = new Bundle;
    bundle.putParcelable(AccountManager.KEY_INTENT, {
      new Intent(context, classOf[MyAuthenticatorActivity]).
        putExtra(MyAuthenticatorActivity.EXTRAS.ACCOUNT_TYPE.toString, accountType).
        putExtra(MyAuthenticatorActivity.EXTRAS.AUTH_TYPE.toString, authTokenType).
        putExtra(MyAuthenticatorActivity.EXTRAS.IS_ADDING_NEW_ACCOUNT.toString, true).
        putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
    });
    bundle;
  }

  def getAuthToken(response: AccountAuthenticatorResponse,
                   account: Account,
                   authTokenType: String,
                   options: Bundle): Bundle = {
    log log "getAuthToken";

    val accountManager = AccountManager.get(context);

    val authToken = accountManager.peekAuthToken(account, authTokenType);

    def genResult(anAuthToken: String): Bundle = {
      val bundle = new Bundle;
      bundle.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
      bundle.putString(AccountManager.KEY_ACCOUNT_TYPE, account.`type`);
      bundle.putString(AccountManager.KEY_AUTHTOKEN, anAuthToken);
      bundle;
    }

    if (TextUtils.isEmpty(authToken)) {
      /*val password = accountManager.getPassword(account);
      if (Option(password).isDefined) {
        log log "lets give another try to authenticate the user";
        val secondTryAuthToken = MyServerAuthenticator.getin(account.name, password, authTokenType);
        genResult(secondTryAuthToken);
      } else {*/
      //if we get here then we couldnt access the user's password - so we need to re-prompt
      //them for their credentials. we create an intent to display our authenticator acitivity
      val bundle = new Bundle;
      bundle.putParcelable(AccountManager.KEY_INTENT, {
        new Intent(context, classOf[MyAuthenticatorActivity]).
          putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response).
          putExtra(MyAuthenticatorActivity.EXTRAS.ACCOUNT_TYPE.toString, account.`type`).
          putExtra(MyAuthenticatorActivity.EXTRAS.AUTH_TYPE.toString, authTokenType);
      });
      bundle;
      //}
    } else {
      genResult(authToken);
    }
  }
}
