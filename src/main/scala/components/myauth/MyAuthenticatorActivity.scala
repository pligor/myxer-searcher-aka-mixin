package components.myauth

import android.accounts.{Account, AccountManager, AccountAuthenticatorActivity}
import com.pligor.myxer_searcher.{R, TR, TypedActivity}
import components.ScalaAsyncTask
import android.content.Intent
import android.app.Activity
import android.os.Bundle
import android.view.View
import components.helpers.log
import components.helpers.AndroidHelper._
import components.myandroid.{BugsenseActivity, MyActivity}
import activities.AdMobActivity

object MyAuthenticatorActivity {

  object EXTRAS extends Enumeration {
    val ACCOUNT_TYPE = Value;
    val AUTH_TYPE = Value;
    val IS_ADDING_NEW_ACCOUNT = Value;
  }

  object OUTPUT_EXTRAS extends Enumeration {
    val STRING_FOR_EXIT = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyAuthenticatorActivity extends AccountAuthenticatorActivity with TypedActivity
with MyActivity
with View.OnClickListener
with AdMobActivity
with BugsenseActivity {
  private val MIN_NAME_LEN = 5;
  private val MIN_PASS_LEN = 5;

  private lazy val ACCOUNT_TYPE = getIntent.getStringExtra(
    MyAuthenticatorActivity.EXTRAS.ACCOUNT_TYPE.toString
  );

  private lazy val isAddingNewAccount = getIntent.getBooleanExtra(
    MyAuthenticatorActivity.EXTRAS.IS_ADDING_NEW_ACCOUNT.toString, false
  );

  private lazy val accountManager = AccountManager.get(this);

  private val authTokenType = MyServerAuthenticator.defaultAuthTokenType;

  private lazy val accountNameEditText = findView(TR.accountNameEditText);
  private lazy val accountPassEditText = findView(TR.accountPassEditText);
  private lazy val accountSubmitButton = findView(TR.accountSubmitButton);

  private var signInTaskOption: Option[SignInTask] = None;

  override def onCreate(icicle: Bundle) {
    super.onCreate(icicle);
    setContentView(R.layout.activity_authenticator);

    accountSubmitButton.setOnClickListener(this);
  }

  override def onStart() {
    super.onStart();
    signInTaskOption = Some(new SignInTask);
  }

  override def onStop() {
    super.onStop();
    signInTaskOption.get.cancel(true);
    signInTaskOption = None;
  }

  def onClick(view: View) {
    view.getId match {
      case R.id.accountSubmitButton => {
        val name = accountNameEditText.getText.toString;
        val pass = accountPassEditText.getText.toString;
        if (name.length < MIN_NAME_LEN || pass.length < MIN_PASS_LEN) {
          showToast(R.string.mail_or_pass_too_short);
        } else {
          assert(signInTaskOption.isDefined);
          signInTaskOption.get.execute(
            Credentials(name, pass)
          );
        }
      }
    }
  }

  private case class Credentials(username: String, password: String);

  private class SignInTask extends ScalaAsyncTask[Credentials, AnyRef, (Credentials, Option[MyServerResponse])] {
    override def onPreExecute() {
      super.onPreExecute();
      Seq(accountNameEditText, accountPassEditText, accountSubmitButton).
        foreach(_.setEnabled(false));
    }

    protected def doInBackground(credentials: Credentials): (Credentials, Option[MyServerResponse]) = {
      (credentials,
        MyServerAuthenticator.getin(credentials.username, credentials.password, authTokenType));
    }

    override def onPostExecute(credsAndServerResponse: (Credentials, Option[MyServerResponse])) {
      super.onPostExecute(credsAndServerResponse);
      val (creds, myServerResponseOption) = credsAndServerResponse;

      if (myServerResponseOption.isDefined) {
        val myServerResponse = myServerResponseOption.get;
        val stringForExitOption = myServerResponse.situation match {
          case Situation.SIGNUP_SUCCESS => {
            assert(!myServerResponse.token.isDefined);
            Some(
              getResources.getString(R.string.signup_success).format(creds.username)
            );
          }
          case Situation.ACTIVATION_PENDING => {
            assert(!myServerResponse.token.isDefined);
            Some(
              getResources.getString(R.string.activation_pending)
            );
          }
          case Situation.SIGNIN_SUCCESS => {
            assert(myServerResponse.token.isDefined);
            None;
          }
          case Situation.USERNAME_EXISTS_PASSWORD_WRONG => {
            assert(!myServerResponse.token.isDefined);
            //TODO fix the server! the non activated for 24hours must be deleted
            Some(
              getResources.getString(R.string.username_exists_password_wrong)
            );
          }
        }

        val intent = (new Intent).
          /*putExtra(AccountManager.KEY_AUTHTOKEN, myServerResponse.token.getOrElse(
          AccountAuthenticator.EMPTY_TOKEN
        )).*/
          putExtra(AccountManager.KEY_ACCOUNT_NAME, creds.username).
          putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);

        if (myServerResponse.token.isDefined) {
          intent.putExtra(AccountManager.KEY_AUTHTOKEN, myServerResponse.token.get);
        }

        if (stringForExitOption.isDefined) {
          intent.putExtra(
            MyAuthenticatorActivity.OUTPUT_EXTRAS.STRING_FOR_EXIT.toString,
            stringForExitOption.get
          );
        } else {
          //do nothing, the extra will be missing
        }

        val accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        val accountPass = creds.password;
        val account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

        if (isAddingNewAccount) {
          //creating the account on the device and setting the auth token we got
          //not setting the auth token will cause another call to the server to authenticate the user
          accountManager.addAccountExplicitly(account, accountPass, null);
        } else {
          accountManager.setPassword(account, accountPass);
        }

        if (myServerResponse.token.isDefined) {
          log log "auth token is set";
          accountManager.setAuthToken(account, authTokenType, myServerResponse.token.get);
        }

        /*
        Now we tell our caller that the process was successful
        could be the Android Account Manager or even our own application
         */

        //val extras = intent.getExtras;
        //log log "size of extras inside my authenticator activity: " + extras.size();

        //inside the following method the key AccountManager.KEY_AUTHTOKEN is being swallowed!
        //we wont get that key (and value) back in the activity
        setAccountAuthenticatorResult(intent.getExtras);
        setResult(Activity.RESULT_OK, intent);
      } else {
        setAccountAuthenticatorResult(new Bundle);
        setResult(Activity.RESULT_CANCELED, null);
      }

      log log "my authenticator activity is finishing";
      finish();
    }
  }

}
