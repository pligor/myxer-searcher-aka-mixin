package components;

import android.os.AsyncTask;

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://developer.android.com/reference/android/os/AsyncTask.html
 * http://blog.nelsonsilva.eu/2009/10/31/scala-on-android-101-proguard-xmlparser-and-function2asynctask
 */
public abstract class ScalaAsyncTask<Params, Progress, Result>
		extends AsyncTask<Params, Progress, Result> {
	@Override
	protected final Result doInBackground(Params... params) {
		return doInBackground(params[0]);
	}

	abstract protected Result doInBackground(Params param);
}
