package components.helpers

import android.content.{ActivityNotFoundException, Context, Intent}
import android.net.Uri
import android.util.Log
import android.app.Activity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object InternetHelper {
  def showWebSite(url: String)(implicit context: Context) {
    val webIntent = new Intent(Intent.ACTION_VIEW);
    webIntent.setData(Uri.parse(url));
    try {
      context.startActivity(webIntent);
    }
    catch {
      case e: ActivityNotFoundException => {
        log log "the given url is NOT prepended with http://";
      }
    }
  }
}
