package components.helpers

import android.bluetooth.BluetoothAdapter
import android.nfc.NfcAdapter
import android.content.Context
import android.util.{Xml, AttributeSet}
import org.xmlpull.v1.XmlPullParser
import android.content.res.Resources
import android.net.{NetworkInfo, ConnectivityManager}
import android.widget.Toast
import android.app.{Activity, ActivityManager}
import scala.collection.JavaConverters._
import android.os.{HandlerThread, Looper}
import java.util.UUID
import android.content.pm.ApplicationInfo

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AndroidHelper {
  def isDebuggable(implicit context: Context): Boolean = {
    val debuggableInt = context.getApplicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE;
    debuggableInt != 0;
  }

  def onUIthread: Boolean = Looper.getMainLooper.getThread == Thread.currentThread();

  def onUI(segment: => Unit)(implicit context: Activity) {
    val activity = context;
    activity.runOnUiThread(new Runnable {
      def run() {
        segment;
      }
    });
  }

  def getHandlerLooper = {
    //create a thread with a looper
    //val busThread = new HandlerThread("AlljoynBusHandler" + UUID.randomUUID());
    val thread = new HandlerThread(UUID.randomUUID().toString);
    //start it
    thread.start();
    //create a handler which runs inside the loop of the looper
    thread.getLooper;
  }

  /**
   * NOT TESTED YET
   */
  def isOn3G(implicit context: Context): Boolean = {
    val info = (context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager]).getActiveNetworkInfo;
    if (info != null && (info.getTypeName.toLowerCase == "mobile")) true;
    else false;
  }

  /**
   * NOT TESTED YET
   */
  def isConnectedOnline(implicit context: Context): Boolean = {
    val connec = context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager];
    try {
      val netInfo: NetworkInfo = connec.getActiveNetworkInfo;
      if (netInfo == null) {
        false;
      } else {
        if (netInfo.getState eq NetworkInfo.State.CONNECTED) {
          true;
        }
        else {
          false;
        }
      }
    }
    catch {
      case e: NullPointerException => {
        false;
      }
    }
  }

  def textToClipboard(key: String, text: String)(implicit context: Context) {
    /*if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
      android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      clipboard.setText("text to clip");
    } else {
      android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      android.content.ClipData clip = android.content.ClipData.newPlainText("text label","text to clip");
      clipboard.setPrimaryClip(clip);
    }*/

    val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE).asInstanceOf[android.text.ClipboardManager];
    clipboardManager.setText(text);
  }

  def getAppNameByPID(pid: Int)(implicit context: Context): Option[String] = {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE).asInstanceOf[ActivityManager];

    val processInfo = manager.getRunningAppProcesses.asScala.find(_.pid == pid);
    if (processInfo.isDefined) {
      Some(processInfo.get.processName);
    } else {
      None;
    }
  }

  def showToast(resourceId: Int)(implicit context: Context) {
    Toast.makeText(context, resourceId, Toast.LENGTH_LONG).show();
  }

  def getAttributeSetFromXml(xmlId: Int, tagName: String, resources: Resources): AttributeSet = {
    /*var state = XmlPullParser.END_DOCUMENT;
    do {
      try {
        state = xmlPullParser.next();
        if (state == XmlPullParser.START_TAG) {
          if (xmlPullParser.getName contains "CardView") {
            Logger(xmlPullParser.getName);
            attrs = Xml.asAttributeSet(xmlPullParser);

            state = XmlPullParser.END_DOCUMENT; //same as break :P
          }
        }
      }
    } while (state != XmlPullParser.END_DOCUMENT);*/

    /**
     * The good thing for being an internal function is that we don't need to pass tagName as a ref
     */
    def getAttributeSet(xmlPullParser: XmlPullParser /*, tagName: String*/): AttributeSet = {
      val state = xmlPullParser.next();
      if (state == XmlPullParser.START_TAG &&
        (xmlPullParser.getName contains tagName)) {
        Xml.asAttributeSet(xmlPullParser);
      }
      else {
        if (state == XmlPullParser.END_DOCUMENT) null;
        else getAttributeSet(xmlPullParser /*, tagName*/);
      }
    }

    getAttributeSet(resources.getXml(xmlId) /*, tagName*/);
  }

  def isWifiConnected(implicit context: Context): Boolean = {
    context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager].
      getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected;
  }

  def isSDcardAvailable: Boolean = {
    android.os.Environment.getExternalStorageState == android.os.Environment.MEDIA_MOUNTED
  }

  def isBluetoothSupported: Boolean = {
    BluetoothAdapter.getDefaultAdapter match {
      case null => false;
      case x: BluetoothAdapter => true;
    }
  }

  /**
   * Note: getDefaultAdapter with no context parameter is deprecated
   */
  def isNFCsupported(implicit context: Context): Boolean = {
    NfcAdapter.getDefaultAdapter(context) match {
      case null => false;
      case x: NfcAdapter => true;
    }
  }
}
