package components.helpers

import java.net.{InetAddress, URLEncoder, MalformedURLException, URL}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object UrlHelper {
  /**
   * As pointed out here: http://stackoverflow.com/questions/7348711/recommended-way-to-get-hostname-in-java
   * this is not the best solution. Try to get it from request .. if available!
   */
  def getHostname = InetAddress.getLocalHost.getHostName;

  def isValidUrl(url: String) = {
    try {
      new URL(url)
      true;
    }
    catch {
      case e: MalformedURLException => false;
    };
  }

  def urlEncode(str: String): String = URLEncoder.encode(str, "UTF-8");
}
