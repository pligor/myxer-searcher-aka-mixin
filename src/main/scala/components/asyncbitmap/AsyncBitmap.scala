package components.asyncbitmap

import android.widget.ImageView
import android.net.Uri

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AsyncBitmap {
  def cancelPotentialWork(imageView: ImageView, uri: Uri): Boolean = {
    val taskOption = grabBitmapWorkerTask(imageView);
    if (taskOption.isDefined) {
      val task = taskOption.get;

      val currentUriOption = task.getCurrentUriOption;
      if (currentUriOption.isDefined) {
        val currentUri = currentUriOption.get;
        if (currentUri == uri) {
          //same work is already in progress
          false;
        } else {
          //cancel previous task
          task.cancel(true);
        }
      } else {
        true;
      }
    } else {
      true;
    }
  }

  //retrieve the task associated with a particular ImageView
  private def grabBitmapWorkerTask(imageView: ImageView): Option[BitmapWorkerTask] = {
    if (Option(imageView).isDefined) {
      val drawable = imageView.getDrawable;
      if (drawable.isInstanceOf[AsyncDrawable]) {
        drawable.asInstanceOf[AsyncDrawable].getBitmapWorkerTaskOption;
      } else {
        None;
      }
    } else {
      None;
    }
  }
}
