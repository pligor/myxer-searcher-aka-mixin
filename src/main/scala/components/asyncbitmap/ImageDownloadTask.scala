package components.asyncbitmap

import android.graphics.{BitmapFactory, Bitmap}
import java.net.{MalformedURLException, URL}
import java.io.{IOException, InputStream}
import android.net.Uri
import components.helpers.log
import components.ScalaAsyncTask
import android.widget.ImageView
import android.content.Context

object ImageDownloader {
  def downloadImage(url: String, imageView: ImageView)(implicit context: Context) {
    val uri = Uri.parse(url);

    if (AsyncBitmap.cancelPotentialWork(imageView, uri)) {
      //if image already has a background task, cancel it
      val task = new BitmapWorkerTask(imageView)({
        imageUriOption: Option[Uri] =>
          if (imageUriOption.isDefined) {
            try {
              if (imageUriOption.isDefined) {
                Some(BitmapFactory.decodeStream(new URL(imageUriOption.get.toString).getContent.asInstanceOf[InputStream]));
              } else {
                None;
              }
            }
            catch {
              case e: MalformedURLException => {
                e.printStackTrace();
                None;
              }
              case e: IOException => {
                e.printStackTrace();
                None;
              }
            }
          } else {
            None;
          }
      });

      //CLEVER TRICK: TO TASK TO APOTHIKEUOUME ENTOS TOU DRAWABLE
      val asyncDrawable = new AsyncDrawable(context.getResources, task);
      //alla mhn ksexname oti kai to task exei reference sto imageview
      //o enas exei reference ston allo diladi :P
      //wste to task na mporei na orisei nea bitmaps sto image view
      //kai to drawable na mporei na kanei cancel to task an auto xreiazetai

      //Pws tha katalavoume oti o enas kai o allos einai ontws zeugari?
      //tha orisoume kati koino kai monadiko, px. to uri

      imageView.setImageDrawable(asyncDrawable);

      //now that are all set up, we can execute task

      task.execute(Some(uri));
    } else {
      log log "same work is already in progress";
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/**
 * Usage:
 * /*new ImageDownloadTask({
      bitmapOption =>
        if (bitmapOption.isDefined) {
          holder.thumbnailImageView.setImageBitmap(bitmapOption.get);
        } else {
          log log "nothing happened";
        }
    }) execute (item.getThumbnail);*/
 * @deprecated
 */
class ImageDownloadTask(op: (Option[Bitmap]) => Unit)
  extends ScalaAsyncTask[String, AnyRef, Option[Bitmap]] {
  protected def doInBackground(url: String): Option[Bitmap] = {
    try {
      val uri = Uri.parse(url);
      log log uri.toString
      Some(BitmapFactory.decodeStream(new URL(uri.toString).getContent.asInstanceOf[InputStream]));
    }
    catch {
      case e: MalformedURLException => {
        e.printStackTrace();
        None;
      }
      case e: IOException => {
        e.printStackTrace();
        None;
      }
    }
  }

  override def onPostExecute(bitmapOption: Option[Bitmap]) {
    super.onPostExecute(bitmapOption);
    op(bitmapOption);
  }
}
