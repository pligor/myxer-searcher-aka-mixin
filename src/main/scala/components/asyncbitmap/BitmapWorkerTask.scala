package components.asyncbitmap

import android.widget.ImageView
import android.net.Uri
import components.ScalaAsyncTask
import scala.ref.WeakReference
import android.graphics.Bitmap

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class BitmapWorkerTask(private val iv: ImageView)(private val op: (Option[Uri]) => Option[Bitmap])
  extends ScalaAsyncTask[Option[Uri], AnyRef, Option[Bitmap]] {

  private val imageViewReference = new WeakReference(iv);

  private var currentUriOption: Option[Uri] = None;
  def getCurrentUriOption = currentUriOption;

  override def onPreExecute() {
    super.onPreExecute();
    /*if (Option(imageView).isDefined) {
      imageView.setVisibility(View.INVISIBLE);
    }*/
  }

  protected def doInBackground(uriOption: Option[Uri]): Option[Bitmap] = {
    currentUriOption = uriOption;
    op(uriOption);
  }

  override def onPostExecute(bitmapOption: Option[Bitmap]) {
    super.onPostExecute(bitmapOption);

    val imageViewOption = imageViewReference.get;
    if ((!isCancelled) && imageViewOption.isDefined && bitmapOption.isDefined) {
      val imageView = imageViewOption.get;
      val bitmap = bitmapOption.get;

      imageView.setImageBitmap(bitmap);

      //imageView.setVisibility(View.VISIBLE);
    } else {
      //TODO
    }
  }
}
