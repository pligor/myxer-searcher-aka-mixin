package components.asyncbitmap

import android.content.res.Resources
import android.graphics.drawable.BitmapDrawable
import scala.ref.WeakReference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

//http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
class AsyncDrawable(res: Resources,
                    private val bwtr: BitmapWorkerTask) extends BitmapDrawable(res) {
  private val bwtReference = new WeakReference(bwtr);

  def getBitmapWorkerTaskOption = bwtReference.get;
}
