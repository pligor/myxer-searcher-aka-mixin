package models

import android.database.Cursor
import components.mydatabase.DatabaseHandler
import com.pligor.myxer_searcher.R
import scala.collection.mutable
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object FavoriteByOrder {
  def getAllDataForList(implicit context: Context): Cursor = {
    val db = (new DatabaseHandler).grabReadableDatabase;
    val c = MyxerItemModel.cols;
    val f = FavoriteItem.cols;
    val query =
      "SELECT " + c.id + " AS _id, " +
        c.name + ", " +
        c.thumbnail + ", " +
        c.artist + ", " +
        c.price + ", " +
        c.`type` + "\n" +
        "FROM " + FavoriteItem.TABLE_NAME + ", " + MyxerItemModel.TABLE_NAME + "\n" +
        "WHERE " + c.id + " = " + f.itemId + "\n" +
        "ORDER BY " + f.ordernum;
    db.rawQuery(query, null);
  }

  private val mapColumnsToFields = {
    val c = MyxerItemModel.cols;
    Map[String, Int](
      c.name.toString -> R.id.favoriteRowName,
      c.artist.toString -> R.id.favoriteRowArtist,
      c.price.toString -> R.id.favoriteRowPrice,
      c.thumbnail.toString -> R.id.favoriteRowThumbnail,
      c.`type`.toString -> R.id.favoriteRowType
    );
  }

  val columnArray = mapColumnsToFields.keys.toArray;

  val fieldArray = mapColumnsToFields.values.toArray;
}
