package models

import android.content.Context
import android.widget.{BaseAdapter, ImageView, TextView}
import android.view.{ViewGroup, View, LayoutInflater}
import com.pligor.myxer_searcher.R
import java.util
import components.asyncbitmap.ImageDownloader._
import scala.collection.JavaConverters._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyxerAdapter(implicit private val context: Context) extends BaseAdapter {
  private var items: util.ArrayList[MyxerItemModel] = null

  def setItems(newItems: util.ArrayList[MyxerItemModel]) {
    items = newItems;
    notifyDataSetChanged();
  }

  private lazy val layoutInflater = LayoutInflater.from(context);

  def getCount: Int = {
    Option(items).map(_.size()).getOrElse(0);
  }

  def getItem(position: Int): MyxerItemModel = {
    if (items == null) {
      null;
    } else {
      items.get(position);
    }
  }

  def getItemId(position: Int): Long = {
    position;
  }

  /**
   * Tutorial: http://xjaphx.wordpress.com/2011/06/16/viewholder-pattern-caching-view-efficiently/ By the above tutorial we are NOT caching the values! We are
   * only caching the TextView, ImageView and other variables that play role for the structure of the view. But NOT the actual values like the actual string
   * in the TextView! Scrolling is very fast!!
   */
  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val view: View = if (convertView == null) {
      val tempView = layoutInflater.inflate(R.layout.item_line_search, null);
      val holder = new ViewHolder(
        nameTextView = tempView.findViewById(R.id.row_name).asInstanceOf[TextView],
        artistTextView = tempView.findViewById(R.id.row_artist).asInstanceOf[TextView],
        thumbnailImageView = tempView.findViewById(R.id.row_thumbnail).asInstanceOf[ImageView],
        priceTextView = tempView.findViewById(R.id.row_price).asInstanceOf[TextView],
        currencySign = tempView.findViewById(R.id.row_currency_sign).asInstanceOf[TextView],
        typeImageView = tempView.findViewById(R.id.row_type).asInstanceOf[ImageView]
      );
      tempView.setTag(holder);
      tempView;
    } else {
      convertView;
    }

    val item = getItem(position);

    val holder = view.getTag.asInstanceOf[ViewHolder]
    val price: Float = item.getPrice
    if (price == 0) {
      holder.priceTextView.setVisibility(View.GONE);
      holder.currencySign.setVisibility(View.GONE);
    }
    else {
      holder.priceTextView.setText(String.valueOf(price))
    }



    downloadImage(item.getThumbnail, holder.thumbnailImageView);


    holder.nameTextView.setText(item.getName)
    holder.artistTextView.setText(item.getArtist)
    holder.typeImageView.setImageResource(item.getDrawableByType)

    val colorId = if ((position % 2) == 0) {
      R.color.listItemEven;
    } else {
      R.color.listItemOdd;
    }

    //Color.parseColor(colorString)
    view.setBackgroundColor(context.getResources.getColor(colorId));

    view;
  }


  private class ViewHolder(
                            val nameTextView: TextView,
                            val artistTextView: TextView,
                            val thumbnailImageView: ImageView,
                            val priceTextView: TextView,
                            val currencySign: TextView,
                            val typeImageView: ImageView
                            );
}
