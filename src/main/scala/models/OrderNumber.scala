package models

import android.content.Context
import components.mydatabase.{MyModel, DatabaseHandler}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait OrderNumber {
  val TABLE_NAME: String;

  val ORDERING_COLUMN: String;

  val INVALID_ORDER_NUMBER = -1;

  val ORDER_LOWER_BOUND = 0;

  def getNextOrderNumber(implicit context: Context): Option[Int] = {
    val colName = "nextOrderNumber";
    val query = "SELECT MAX(" + ORDERING_COLUMN + ")+1 AS " + colName + " FROM " + TABLE_NAME;

    (new DatabaseHandler).getSingleScalar[Int](query);
  }

  def getTop(implicit context: Context): Option[MyModel];
}
