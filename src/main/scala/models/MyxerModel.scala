package models

import android.content.Context
import com.pligor.myxer_searcher.R
import java.net.URLEncoder
import scala.collection.mutable
import scala.io.Codec

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyxerModel {
  val defaultCount: Short = 10;

  /**
   * http://www.myxer.com/api/doc/feed_search/
   */
  final val filters = Array("category", "pricing", "type", "format", "filter", "count");
  private final val url_prefixString = "http://api.myxer.com/feeds/search/"
  private final val format_index: Int = 3;

  private def getFormat(implicit context: Context): String = {
    val formats = context.getResources.getStringArray(R.array.format);
    formats(MyxerModel.format_index);
  }

  /**
   * sample: http://api.myxer.com/feeds/search/Riley/?type=ringtone&pricing=free
   *
   * @return String
   */
  def getApiUri(map: mutable.Map[String, String])(implicit context: Context): String = {
    val charsetName = "UTF-8";
    var url_postfixString: String = URLEncoder.encode(map.get("search").get, charsetName) + "/?"
    map.put("format", getFormat);
    MyxerModel.filters foreach {
      filter =>
        if (map.contains(filter)) {
          url_postfixString += filter + "=" + URLEncoder.encode(map.get(filter).get, charsetName) + "&"
        }
    }
    MyxerModel.url_prefixString + url_postfixString
  }
}


