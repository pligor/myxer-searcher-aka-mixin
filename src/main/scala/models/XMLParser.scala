package models

import org.xml.sax.helpers.DefaultHandler
import android.content.Context
import java.util
import scala.collection.mutable
import org.xml.sax.{InputSource, XMLReader, Attributes}
import javax.xml.parsers.{SAXParser, SAXParserFactory}
import java.io.StringReader
import components.ScalaAsyncTask

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

class XMLParsingTask[T](op: (util.ArrayList[T]) => Unit)(implicit context: Context)
  extends ScalaAsyncTask[String, AnyRef, util.ArrayList[T]] {
  protected def doInBackground(xml: String): util.ArrayList[T] = {
    try {
      val reader = SAXParserFactory.newInstance.newSAXParser.getXMLReader;

      val parser = new XMLParser();

      reader.setContentHandler(parser);

      val inStream = new InputSource;
      inStream.setCharacterStream(new StringReader(xml));

      reader.parse(inStream);

      parser.getList.asInstanceOf[util.ArrayList[T]];
    }
    catch {
      case e: Exception => {
        e.printStackTrace();
        return null;
      }
    }
  }

  override def onPostExecute(items: util.ArrayList[T]) {
    super.onPostExecute(items);
    op(items);
  }
}

class XMLParser(implicit val context: Context) extends DefaultHandler {
  private val myxerItemList = new util.ArrayList[MyxerItemModel];
  private var model: MyxerItemModel = null
  private val check = mutable.Map.empty[String, Boolean];

  clearCheck();

  private def clearCheck() {
    check.put("item", false);
    val attributes: Array[String] = MyxerItemModel.attributes

    attributes.foreach(check.put(_, false));
  }

  def getList = myxerItemList;

  /** Gets be called on opening tags like: <tag> **/
  override def startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
    if (localName.contentEquals("item")) {
      model = new MyxerItemModel();
    }
    check.put(localName, true)
  }

  override def characters(ch: Array[Char], start: Int, length: Int) {
    val string: String = new String(ch, start, length);
    if (check("id")) {
      model = model.setIdFromString(string);
    }
    else if (check("name")) {
      model.appendName(string);
    }
    else if (check("price")) {
      model.setPrice(string);
    }
    else if (check("itemlink")) {
      model.appendItemlink(string);
    }
    else if (check("itemsendlink")) {
      model.appendItemsendlink(string);
    }
    else if (check("artist")) {
      model.appendArtist(string);
    }
    else if (check("profilelink")) {
      model.appendProfilelink(string)
    }
    else if (check("type")) {
      model.appendType(string);
    }
    else if (check("sendlink")) {
      model.appendSendlink(string);
    }
    else if (check("created")) {
      model.setCreated(string);
    }
    else if (check("preview")) {
      model.appendPreview(string);
    }
    else if (check("smallthumbnail")) {
      model.appendSmallthumbnail(string);
    }
    else if (check("thumbnail")) {
      model.appendThumbnail(string);
    }
    else if (check("bigthumbnail")) {
      model.appendBigthumbnail(string);
    }
  }

  /**
   * Gets be called on closing tags like: </tag>
   */
  override def endElement(namespaceURI: String, localName: String, qName: String) {
    if (localName.contentEquals("item")) {
      if (model != null) {
        myxerItemList.add(model)
      }
    }
    check.put(localName, false);
  }
}


