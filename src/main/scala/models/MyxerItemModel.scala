package models

import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import com.pligor.myxer_searcher.R
import android.util.Log
import android.content.{ContentValues, Context}
import android.net.ParseException
import components.helpers.log
import components.mydatabase.{DatabaseHandler, MyModelObject, MyModel}
import java.lang
import android.database.Cursor

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * OK... this is confusing :P String buffers are safe for use by multiple threads. The methods are synchronized where necessary so that all the operations on
 * any particular instance behave as if they occur in some serial order that is consistent with the order of the method calls made by each of the individual
 * threads involved
 *
 * @author pligor
 */
object MyxerItemModel extends MyModelObject {
  val TABLE_NAME: String = "myxeritemmodel";

  def createFromCursor(cursor: Cursor): MyxerItemModel = {
    val model = new MyxerItemModel(Some(cursor.getLong(cursor.getColumnIndexOrThrow(cols.id.toString))));
    model.setPrice(cursor.getFloat(cursor.getColumnIndexOrThrow(cols.price.toString)));
    model.setCreated(cursor.getLong(cursor.getColumnIndexOrThrow(cols.created.toString)));
    model.setName(cursor.getString(cursor.getColumnIndexOrThrow(cols.name.toString)));
    model.setItemlink(cursor.getString(cursor.getColumnIndexOrThrow(cols.itemlink.toString)));
    model.setItemsendlink(cursor.getString(cursor.getColumnIndexOrThrow(cols.itemsendlink.toString)));
    model.setArtist(cursor.getString(cursor.getColumnIndexOrThrow(cols.artist.toString)));
    model.setProfilelink(cursor.getString(cursor.getColumnIndexOrThrow(cols.profilelink.toString)));
    model.setType(cursor.getString(cursor.getColumnIndexOrThrow(cols.`type`.toString)));
    model.setSendlink(cursor.getString(cursor.getColumnIndexOrThrow(cols.sendlink.toString)));
    model.setPreview(cursor.getString(cursor.getColumnIndexOrThrow(cols.preview.toString)));
    model.setSmallthumbnail(cursor.getString(cursor.getColumnIndexOrThrow(cols.smallthumbnail.toString)))
    model.setThumbnail(cursor.getString(cursor.getColumnIndexOrThrow(cols.thumbnail.toString)));
    model.setBigthumbnail(cursor.getString(cursor.getColumnIndexOrThrow(cols.bigthumbnail.toString)));
    model;
  }

  case object cols extends Enumeration {
    val id = Value;
    val price = Value;
    val created = Value;
    val name = Value;
    val itemlink = Value;
    val itemsendlink = Value;
    val artist = Value;
    val profilelink = Value;
    val `type` = Value;
    val sendlink = Value;
    val preview = Value;
    val smallthumbnail = Value;
    val thumbnail = Value;
    val bigthumbnail = Value;
  }

  val formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("el"))
  val attributes: Array[String] = Array("id", "name", "price", "itemlink", "itemsendlink", "artist", "profilelink", "type", "sendlink", "created", "preview", "smallthumbnail", "thumbnail", "bigthumbnail");

  override def getAll(implicit context: Context): Seq[MyxerItemModel] = super.getAll(context).map(_.asInstanceOf[MyxerItemModel]);

  //def getById[Α <: MyModel](modelId: Int)(implicit context: Context): Option[Α] = {
  override def getById(modelId: Long)(implicit context: Context): Option[MyxerItemModel] = {
    try {
      super.getById(modelId)(context).asInstanceOf[Option[MyxerItemModel]];
    } catch {
      case e: IllegalArgumentException => {
        //typical when db row exists but the file is missing
        None;
      }
    }
  }

  def getByIds(ids: Seq[Long])(implicit context: Context): Seq[MyxerItemModel] = {
    val binders = DatabaseHandler.genBinders(ids.length);

    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM myxeritemmodel
        |WHERE id IN (%s)""".stripMargin.format(binders), ids.map(_.toString).toArray
    )(createFromCursor);
  }

  def getDrawableByType(`type`: String): Int = {
    `type`.toLowerCase match {
      case "ringtone" => R.drawable.ringtone_icon;
      case "wallpaper" => R.drawable.wallpaper_icon;
      case "song" | "mp3" => R.drawable.song_icon;
      case "video" => R.drawable.video_icon;
      case _ => 0;
    }
  }
}

/**
 * @param id we get the id from online, so it is NOT auto increment
 */
class MyxerItemModel(id: Option[Long] = None) extends MyModel(id) {

  def makeFavorite(rating: Float)(implicit context: Context) {
    require(id.isDefined);
    val favoriteItemOption = FavoriteItem.getById(id.get);
    val finalModel = new FavoriteItem(rating = rating, itemId = id);
    if (favoriteItemOption.isDefined) {
      assert(finalModel.update);
      log log "setting the rating for consecutive time";
    } else {
      assert(finalModel.insert != DatabaseHandler.invalidInsertId);
      log log "setting the rating for a first time";
    }
  }

  def removeFromFavorites(implicit context: Context) {
    val favoriteItemOption = FavoriteItem.getById(id.get);
    if (favoriteItemOption.isDefined) {
      assert(favoriteItemOption.get.delete);
      log log "item is removed from favorites";
    } else {
      log log "nothing inside favorites table, nothing to clean up";
    }
  }

  def getRating(implicit context: Context): Option[Float] = {
    require(id.isDefined);
    FavoriteItem.getById(id.get).map(_.rating);
  }

  def setIdFromString(idString: String): MyxerItemModel = {
    val model = new MyxerItemModel(Some(idString.toLong));
    model.copyFromAnother(this);
    model;
  }

  def copyFromAnother(model: MyxerItemModel) {
    setPrice(model.getPrice);
    setCreated(model.getCreated.getTime);
    setName(model.getName);
    setItemlink(model.getItemlink);
    setItemsendlink(model.getItemsendlink);
    setArtist(model.getArtist);
    setProfilelink(model.getProfilelink);
    setType(model.getType);
    setSendlink(model.getSendlink);
    setPreview(model.getPreview);
    setSmallthumbnail(model.getSmallthumbnail);
    setThumbnail(model.getThumbnail);
    setBigthumbnail(model.getBigthumbnail);
  }

  val TABLE_NAME: String = MyxerItemModel.TABLE_NAME;

  val ID_COLUMN_NAME: String = MyxerItemModel.ID_COLUMN_NAME;

  def isIdAutoIncrement: Boolean = false;

  override def insert(implicit context: Context) = {
    require(id.isDefined);
    super.insert;
  }

  protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues();

    require(id.isDefined);
    contentValues.put(MyxerItemModel.cols.id.toString, new lang.Long(id.get));

    contentValues.put(MyxerItemModel.cols.price.toString, new lang.Float(getPrice));
    contentValues.put(MyxerItemModel.cols.created.toString, new lang.Long(getCreated.getTime));
    contentValues.put(MyxerItemModel.cols.name.toString, getName);
    contentValues.put(MyxerItemModel.cols.itemlink.toString, getItemlink);
    contentValues.put(MyxerItemModel.cols.itemsendlink.toString, getItemsendlink);
    contentValues.put(MyxerItemModel.cols.artist.toString, getArtist);
    contentValues.put(MyxerItemModel.cols.profilelink.toString, getProfilelink);
    contentValues.put(MyxerItemModel.cols.`type`.toString, getType);
    contentValues.put(MyxerItemModel.cols.sendlink.toString, getSendlink);
    contentValues.put(MyxerItemModel.cols.preview.toString, getPreview);
    contentValues.put(MyxerItemModel.cols.smallthumbnail.toString, getSmallthumbnail);
    contentValues.put(MyxerItemModel.cols.thumbnail.toString, getThumbnail);
    contentValues.put(MyxerItemModel.cols.bigthumbnail.toString, getBigthumbnail);
    contentValues;
  }

  private var itsPrice: Float = 0.toFloat;
  private var itsCreated: Date = new Date;

  private val itsName: StringBuffer = new StringBuffer;
  private val itsItemlink: StringBuffer = new StringBuffer;
  private val itsItemsendlink: StringBuffer = new StringBuffer;
  private val itsArtist: StringBuffer = new StringBuffer;
  private val itsProfilelink: StringBuffer = new StringBuffer;
  private val itsType: StringBuffer = new StringBuffer;
  private val itsSendlink: StringBuffer = new StringBuffer;
  private val itsPreview: StringBuffer = new StringBuffer;
  private val itsSmallthumbnail: StringBuffer = new StringBuffer;
  private val itsThumbnail: StringBuffer = new StringBuffer;
  private val itsBigthumbnail: StringBuffer = new StringBuffer;

  def getDrawableByType: Int = MyxerItemModel.getDrawableByType(getType);

  def getName = itsName.toString;

  def getPrice = itsPrice;

  def getItemlink = itsItemlink.toString;

  def getItemsendlink = itsItemsendlink.toString;

  def getArtist = itsArtist.toString;

  def getProfilelink = itsProfilelink.toString;

  def getType = itsType.toString;

  def getSendlink = itsSendlink.toString;

  def getCreated = itsCreated;

  def getPreview = itsPreview.toString;

  def getSmallthumbnail = itsSmallthumbnail.toString;

  def getThumbnail = itsThumbnail.toString;

  def getBigthumbnail = itsBigthumbnail.toString;

  def setPrice(string: String) {
    itsPrice = string.toFloat;
  }

  def setPrice(price: Float) {
    itsPrice = price;
  }

  def setCreated(timestamp: Long) {
    itsCreated = new Date(timestamp);
  }

  def setCreated(string: String) {
    try {
      itsCreated = MyxerItemModel.formatter.parse(string)
    }
    catch {
      case e: ParseException => {
        e.printStackTrace();
        Log.i("pl", "Parsing the date has failed")
      }
    }
  }

  def setName(string: String) {
    itsName.setLength(0);
    appendName(string);
  }

  def appendName(string: String) {
    itsName.append(string);
  }

  def setItemlink(string: String) {
    itsItemlink.setLength(0);
    appendItemlink(string);
  }

  def appendItemlink(string: String) {
    itsItemlink.append(string);
  }

  def setItemsendlink(string: String) {
    itsItemsendlink.setLength(0);
    appendItemsendlink(string);
  }

  def appendItemsendlink(string: String) {
    itsItemsendlink.append(string);
  }

  def setArtist(string: String) {
    itsArtist.setLength(0);
    appendArtist(string);
  }

  def appendArtist(string: String) {
    itsArtist.append(string);
  }

  def setProfilelink(string: String) {
    itsProfilelink.setLength(0);
    appendProfilelink(string);
  }

  def appendProfilelink(string: String) {
    itsProfilelink.append(string);
  }

  def setType(string: String) {
    itsType.setLength(0);
    appendType(string);
  }

  def appendType(string: String) {
    itsType.append(string);
  }

  def setSendlink(string: String) {
    itsSendlink.setLength(0);
    appendSendlink(string);
  }

  def appendSendlink(string: String) {
    itsSendlink.append(string);
  }

  def setPreview(string: String) {
    itsPreview.setLength(0);
    appendPreview(string);
  }

  def appendPreview(string: String) {
    itsPreview.append(string);
  }

  def setSmallthumbnail(string: String) {
    itsSmallthumbnail.setLength(0);
    appendSmallthumbnail(string);
  }

  def appendSmallthumbnail(string: String) {
    itsSmallthumbnail.append(string);
  }

  def setThumbnail(string: String) {
    itsThumbnail.setLength(0);
    appendThumbnail(string);
  }

  def appendThumbnail(string: String) {
    itsThumbnail.append(string);
  }

  def setBigthumbnail(string: String) {
    itsBigthumbnail.setLength(0);
    appendBigthumbnail(string);
  }

  def appendBigthumbnail(string: String) {
    itsBigthumbnail.append(string);
  }
}


