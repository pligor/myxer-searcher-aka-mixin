package models

import components.mydatabase.{DatabaseHandler, MyModel, MyModelObject}
import android.content.{ContentValues, Context}
import android.database.Cursor
import java.lang
import android.database.sqlite.SQLiteDatabase
import scala.collection.mutable.ListBuffer
import components.helpers.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */


object FavoriteItem extends MyModelObject with OrderNumber {
  val ORDERING_COLUMN: String = cols.ordernum.toString;

  def getTop(implicit context: Context): Option[FavoriteItem] = {
    //TODO implement this
    throw new Exception("not implemented yet");
  }

  /**
   * BE AWARE: We should not use our helper methods of DatabaseHandler because these open new db connections.
   * We do not want to do that
   */
  def introduceOrdering(db: SQLiteDatabase) {
    log log "add ordernum column";

    val addOrderNumColumn = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + cols.ordernum + " INT NOT NULL DEFAULT 0";
    db.execSQL(addOrderNumColumn);

    log log "initialize each row with the order number";

    val ids = {
      val cursor = db.rawQuery("SELECT " + cols.itemId + " FROM " + TABLE_NAME, null);
      cursor.moveToFirst();
      if (cursor.moveToFirst()) {
        val listBuffer = ListBuffer.empty[Long];
        do {
          listBuffer += cursor.getLong(cursor.getColumnIndexOrThrow(cols.itemId.toString));
        } while (cursor.moveToNext());
        listBuffer.toSeq;
      } else {
        Seq.empty[Long];
      }
    }
    val contentValues = new ContentValues;
    var counter = 0;
    ids.foreach {
      id =>
        contentValues.put(cols.ordernum.toString, new lang.Long(counter));
        val whereClause = cols.itemId + " = ?";
        val whereArgs = Array[String](id.toString);
        db.update(TABLE_NAME, contentValues, whereClause, whereArgs);
        counter += 1;
    }

    log log "add unique index to the ordernum column";

    db.execSQL("CREATE UNIQUE index unique_" + cols.ordernum + " on " + TABLE_NAME + "(" + cols.ordernum + ")");
  }

  val FAVORITE_THRESHOLD = 1.0F;

  val DEFAULT_RATING = 0F;

  val TABLE_NAME = "favoriteitem";
  override val ID_COLUMN_NAME = "itemId";

  case object cols extends Enumeration {
    val itemId = Value;
    val rating = Value;
    val ordernum = Value;
  }

  def isEmpty(implicit context: Context) = (getCount == 0);

  def createFromCursor(cursor: Cursor) = new FavoriteItem(
    itemId = Some(cursor.getLong(cursor.getColumnIndexOrThrow(cols.itemId.toString))),
    rating = cursor.getFloat(cursor.getColumnIndexOrThrow(cols.rating.toString)),
    ordernum = cursor.getLong(cursor.getColumnIndexOrThrow(cols.ordernum.toString))
  );

  override def getAll(implicit context: Context): Seq[FavoriteItem] = super.getAll(context).map(_.asInstanceOf[FavoriteItem]);

  override def getById(modelId: Long)(implicit context: Context): Option[FavoriteItem] = {
    try {
      super.getById(modelId)(context).asInstanceOf[Option[FavoriteItem]];
    } catch {
      case e: IllegalArgumentException => {
        None;
      }
    }
  }

  def getAllIds(implicit context: Context) = getAll.map(favItem => new java.lang.Long(favItem.id.get));

  def getHigherThan(orderNum: Long)(implicit context: Context): Seq[FavoriteItem] = {
    (new DatabaseHandler).getModelsByQuery(
      """
        |SELECT *
        |FROM favoriteitem
        |WHERE ordernum > ?
        |ORDER BY ordernum DESC
      """.stripMargin, Array[String](orderNum.toString)
    )(createFromCursor);
  }

  def getFromOrderUntilOrder(fromOrder: Long, untilOrder: Long)(implicit context: Context): Seq[FavoriteItem] = {
    (new DatabaseHandler).getModelsByQuery(
      """
        |SELECT *
        |FROM favoriteitem
        |WHERE ordernum >= ? AND ordernum < ?
        |ORDER BY ordernum DESC
      """.stripMargin, Array[String](fromOrder.toString, untilOrder.toString)
    )(createFromCursor);
  }

  /*def incByOneForOrderedHigherThan(ordernum: Long)(implicit context: Context) {
    (new DatabaseHandler).workWithWritableDatabase {
      db =>
        db.execSQL(
          """UPDATE favoriteitem
            |SET ordernum = ordernum + 1
            |WHERE ordernum > ?""".stripMargin, Array[AnyRef](ordernum.toString));
    }
  }

  def incByOneOrderedFromUntil(fromOrder: Long, untilOrder: Long)(implicit context: Context) {
    (new DatabaseHandler).workWithWritableDatabase {
      db =>
        db.execSQL(
          """UPDATE favoriteitem
            |SET ordernum = ordernum + 1
            |WHERE ordernum >= ? AND ordernum < ?
          """.stripMargin, Array[AnyRef](fromOrder.toString, untilOrder.toString));
    }
  }*/
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class FavoriteItem(val rating: Float,
                   val ordernum: Long,
                   itemId: Option[Long] = None) extends MyModel(itemId) {
  val TABLE_NAME = FavoriteItem.TABLE_NAME;
  val ID_COLUMN_NAME = FavoriteItem.ID_COLUMN_NAME;

  def this(rating: Float, itemId: Option[Long])(implicit context: Context) {
    this(rating, {
      val nextOrderNumber = FavoriteItem.getNextOrderNumber(context);
      if (nextOrderNumber.isDefined) {
        nextOrderNumber.get;
      } else {
        FavoriteItem.ORDER_LOWER_BOUND;
      }
    }, itemId);
  }

  protected def generateContentValues: ContentValues = {
    require(itemId.isDefined);
    val contentValues = new ContentValues();
    contentValues.put(FavoriteItem.cols.itemId.toString, new java.lang.Long(itemId.get));
    contentValues.put(FavoriteItem.cols.rating.toString, new lang.Float(rating));
    contentValues.put(FavoriteItem.cols.ordernum.toString, new java.lang.Long(ordernum));
    contentValues;
  }

  def isIdAutoIncrement: Boolean = false;

  def setOrdernum(newOrdernum: Long) = new FavoriteItem(
    rating = rating,
    ordernum = newOrdernum,
    itemId = itemId
  );
}
