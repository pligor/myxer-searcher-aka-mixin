package activities

import scala.collection.JavaConverters._
import android.app.Activity
import android.view.{View, Gravity}
import com.pligor.myxer_searcher._
import android.widget._
import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import components.helpers.log
import models.{FavoriteItem, MyxerItemModel, FavoriteByOrder}
import components.myandroid.{MyActivity, BugsenseActivity}
import com.bugsense.trace.BugSenseHandler
import com.terlici.dragndroplist.{DragNDropCursorAdapter, DragNDropListView}
import components.helpers.AndroidHelper._
import components.WriteOnce
import android.database.{CursorWrapper, Cursor}
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder
import components.asyncbitmap.ImageDownloader._
import com.terlici.dragndroplist.DragNDropListView.OnItemDragNDropListener
import scala.Some

object FavoriteListActivity {

  object Extras extends Enumeration {
    val ID_LIST = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * https://github.com/terlici/DragNDropList
 *
 * http://stackoverflow.com/questions/8510335/simplecursoradapter-with-imageview-and-textview
 */
class FavoriteListActivity extends Activity with TypedActivity with OnItemClickListener
with MyActivity
with OnItemDragNDropListener
with AdMobActivity
with BugsenseActivity {
  private var cursor: Option[Cursor] = None;

  private lazy val favoriteTitleTextView: TextView = findView(TR.favoriteTitleTextView);
  private lazy val favoriteItemListView: DragNDropListView = findView(TR.favoriteItemListView);

  private val adapter = new WriteOnce[DragNDropCursorAdapter];

  private lazy val viewBinder = new ViewBinder {
    def setViewValue(view: View, cursor: Cursor, columnIndex: Int): Boolean = {
      view.getId match {
        case R.id.favoriteRowPrice => {
          val curPrice = cursor.getFloat(columnIndex);
          if (curPrice == 0) {
            view.setVisibility(View.GONE);
          } else {
            view.setVisibility(View.VISIBLE);
            view.asInstanceOf[TextView].setText(String.valueOf(curPrice) + getResources.getString(R.string.row_currency_sign_default));
          }
          true;
        }
        case R.id.favoriteRowThumbnail => {
          downloadImage(url = cursor.getString(columnIndex), view.asInstanceOf[ImageView]);
          true;
        }
        case R.id.favoriteRowType => {
          view.asInstanceOf[ImageView].setImageResource(
            MyxerItemModel.getDrawableByType(cursor.getString(columnIndex))
          );
          true;
        }
        case _ => false;
      }
    }
  }

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    //both done from manifest
    //requestWindowFeature(Window.FEATURE_NO_TITLE)
    //getWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    setContentView(R.layout.activity_favorite_list);

    favoriteTitleTextView.setText(R.string.list_title_on_favorites);

    favoriteItemListView.setOnItemClickListener(this);

    val curCursor = resetCursor();

    if (new CursorWrapper(curCursor).getCount == 0) {
      noItems();
    } else {
      adapter setValue new DragNDropCursorAdapter(
        context,
        R.layout.item_line_favorite,
        curCursor,
        FavoriteByOrder.columnArray, //the columns here, match in order..
        FavoriteByOrder.fieldArray, //..the fields here
        R.id.favoriteRowThumbnail
      );

      adapter().setViewBinder(viewBinder);

      favoriteItemListView.setDragNDropAdapter(adapter());

      favoriteItemListView.setOnItemDragNDropListener(this);

      Toast.makeText(this,R.string.favorite_list_help, Toast.LENGTH_SHORT).show();
    }
  }

  def resetCursor(): Cursor = {
    cursor = Some(FavoriteByOrder.getAllDataForList);
    cursor.get;
  }

  override def onRestart() {
    super.onRestart();
    resetList();
  }

  def resetList() {
    adapter().swapCursor(resetCursor()).close();
  }

  override def onStart() {
    super.onStart();
    //TODO make it perhaps more specific with certain ids? although this confuses things with ordernums!
    /*val ids = getIntent.getExtras.getStringArray(FavoriteListActivity.Extras.ID_LIST.toString);
    assert(Option(ids).isDefined);
    val items = MyxerItemModel.getByIds(ids.map(_.toLong));
    myxerAdapter.setItems(new java.util.ArrayList(items.asJava));*/
  }

  override def onStop() {
    super.onStop();
    assert(cursor.isDefined && !cursor.get.isClosed);
    cursor.get.close();
  }

  private def noItems() {
    showToast(R.string.no_items_found);
    //toast.setGravity(Gravity.CENTER, 0, 0);
    val intent = new Intent(this, classOf[MainActivity]);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  def onItemDrag(parent: DragNDropListView, view: View, position: Int, id: Long) {}

  /**
   * If we acquire the models, we get them as they were before drop
   */
  def onItemDrop(parent: DragNDropListView, view: View, startPosition: Int, endPosition: Int, id: Long) {
    /*val modelStart = MyxerItemModel.getById(id).get;
    val modelEnd = MyxerItemModel.getById(adapter().getItemId(endPosition)).get;
    log log "startPosition: " + startPosition;
    log log "modelStart artist: " + modelStart.getArtist;
    log log "modelStart id: " + modelStart.id.get;
    log log "endPosition: " + endPosition;
    log log "modelEnd artist: " + modelEnd.getArtist;
    log log "modelEnd id: " + modelEnd.id.get;
    log log "id: " + id;*/

    val favoriteStart = FavoriteItem.getById(id).get;
    val favoriteEnd = FavoriteItem.getById(adapter().getItemId(endPosition)).get;

    if (startPosition < endPosition) {
      val targetOrderNum = favoriteEnd.ordernum;

      //FavoriteItem.incByOneForOrderedHigherThan(targetOrderNum);
      val favoriteItems = FavoriteItem.getHigherThan(targetOrderNum);
      favoriteItems foreach {
        favoriteItem =>
          assert(favoriteItem.setOrdernum(favoriteItem.ordernum + 1).update);
      }

      val newOrderNum = targetOrderNum + 1;
      if (favoriteStart.setOrdernum(newOrderNum).update) {
        log log "moved from lower to higher position successfully";
      } else {
        resetList();
      }
    } else if (startPosition > endPosition) {
      val untilOrderNum = favoriteStart.ordernum;

      if (favoriteStart.setOrdernum(FavoriteItem.INVALID_ORDER_NUMBER).update) {
        val fromOrderNum = favoriteEnd.ordernum;
        val favoriteItems = FavoriteItem.getFromOrderUntilOrder(fromOrderNum, untilOrderNum);
        favoriteItems foreach {
          favoriteItem =>
            assert(favoriteItem.setOrdernum(favoriteItem.ordernum + 1).update);
        }
        val newOrderNum = fromOrderNum;
        if (favoriteStart.setOrdernum(newOrderNum).update) {
          log log "moved from higher to lower position successfully";
        } else {
          resetList();
        }
      } else {
        resetList();
      }
    } else {
      log log "do not do anything if positions are the same";
    }
  }

  def onItemClick(parentView: AdapterView[_], view: View, position: Int, id: Long) {
    log log "position: " + position;
    log log "id: " + id;
    val itemOption = MyxerItemModel.getById(id);
    if (itemOption.isDefined) {
      MyxerItemActivity setItem itemOption.get;
      startActivity(new Intent(this, classOf[MyxerItemActivity]));
    } else {
      log log "item was found null with clicked id: " + id.toInt;
      BugSenseHandler.sendEvent("on item click the item found was null. id: " + id + " and position: " + position);
    }
  }
}
