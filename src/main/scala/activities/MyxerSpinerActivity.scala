package activities

import android.app.Activity
import com.pligor.myxer_searcher.{TR, TypedActivity, R}
import android.widget.{Toast, TextView, ProgressBar}
import android.os.Bundle
import android.view.{View, Gravity}
import android.util.Log
import android.content.Intent
import scala.collection.mutable
import scala.collection.JavaConverters._
import scala.collection.immutable.HashMap
import components.{InternetFetchTask, ScalaAsyncTask}
import models.MyxerModel
import components.helpers.log
import components.helpers.AndroidHelper._
import components.myandroid.BugsenseActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyxerSpinerActivity extends Activity with TypedActivity
with AdMobActivity
with BugsenseActivity {
  implicit protected val context = this;

  private lazy val fetchDataSpiner: ProgressBar = findView(TR.fetchDataSpiner);
  private lazy val spinerTextView: TextView = findView(TR.spinerTextView);

  private var fetchData: Option[InternetFetchTask] = None;

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    this.setContentView(R.layout.activity_myxer_spiner);
  }

  override def onStart() {
    super.onStart();

    fetchData = Some(new InternetFetchTask(dataCallback))
    val receivedBundle = getIntent.getExtras;
    fetchData.get.execute(getApiUri(receivedBundle));
  }

  override def onStop() {
    super.onStop();
    if (fetchData.isDefined) {
      fetchData.get.cancel(true);
    }
  }

  private def dataCallback(dataOption: Option[String]) {
    //log log dataOption;
    if(dataOption.isDefined) {
      val intent = new Intent(this, classOf[MyxerListActivity]);
      intent.putExtra(MyxerListActivity.Extras.RAW_XML.toString, dataOption.get);
      startActivity(intent);
    } else {
      showToast(R.string.no_connection);
      finish();
    }
  }

  private def getApiUri(receivedBundle: Bundle) = {
    MyxerModel.getApiUri(convertBundleOfStringsToHashMap(receivedBundle));
    //Log.i("pl", "apiUri: " + apiUri)
  }

  private def convertBundleOfStringsToHashMap(bundle: Bundle): mutable.HashMap[String, String] = {
    val map = new mutable.HashMap[String, String]
    val keySet: Set[String] = bundle.keySet.asScala.toSet;
    for (cur_key <- keySet) {
      map.put(cur_key, bundle.getString(cur_key))
    }
    map;
  }
}


