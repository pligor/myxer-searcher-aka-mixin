package activities

import scala.collection.JavaConverters._
import android.app.Activity
import com.google.ads.{AdRequest, Ad, AdListener, AdView}
import com.pligor.myxer_searcher.{R, TypedActivity, TR}
import com.google.ads.AdRequest.ErrorCode
import android.view.View
import components.myandroid.MyAnimator
import components.helpers.log
import android.os.Bundle

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AdMobActivity extends Activity with TypedActivity {
  private val reminderDuration = 10000;
  //msec
  private val reminderProbability = 1.0 / 10.0;
  //~6%

  private lazy val myAdView = findViewById(R.id.myAdView).asInstanceOf[AdView];
  private lazy val reminderLayout = findView(TR.reminderLayout);

  /*private val adRequest = {
    val adr = new AdRequest;
    adr.setTestDevices(Set(
      AdRequest.TEST_EMULATOR,
      "015d2d42211bf80c",
      "BX903B3T3Q"
    ).asJava);
    adr;
  }*/


  override def onPostCreate(savedInstanceState: Bundle) {
    super.onPostCreate(savedInstanceState);

    if (Option(myAdView).isDefined) {
      myAdView.setAdListener(adListener);
      //myAdView.loadAd(adRequest);
      log log "advertisement is trying to load now";
    } else {
      log log "something happened and the myAdView is no longer available";
    }
  }

  private object adListener extends AdListener {
    def onDismissScreen(ad: Ad) {}

    def onFailedToReceiveAd(ad: Ad, errorCode: ErrorCode) {}

    def onReceiveAd(ad: Ad) {
      if (math.random < reminderProbability) {
        log log "the reminder is going to be displayed";
        reminderLayout.setVisibility(View.VISIBLE);
        reminderLayout.setAnimation(MyAnimator.fadeInAndOut(reminderDuration) {
          reminderLayout.setVisibility(View.GONE);
        });
      } else {
        log log "the reminder is NOT going to be displayed";
      }
    }

    def onLeaveApplication(ad: Ad) {}

    def onPresentScreen(ad: Ad) {}
  }

}
