package activities

import android.view.View.OnClickListener
import android.widget._
import android.content.Intent
import com.pligor.myxer_searcher._
import android.view.View
import android.app.Activity
import scala.collection.JavaConverters._
import android.os.Bundle
import components.helpers.log
import models.FavoriteItem
import components.myandroid.BugsenseActivity
import components.helpers.InternetHelper._

class MainActivity extends Activity with TypedActivity
with OnClickListener
with AdMobActivity
with AuthorizedActivity
with BugsenseActivity {
  private implicit val context = this;

  private lazy val wallpaperButton: ImageButton = findView(TR.wallpaperButton);
  private lazy val videoButton: ImageButton = findView(TR.videoButton);
  private lazy val ringtoneButton: ImageButton = findView(TR.ringtoneButton);
  private lazy val songButton: ImageButton = findView(TR.songButton);
  private lazy val hitMeButton: Button = findView(TR.hitMeButton);
  private lazy val favoritesButton: Button = findView(TR.favoritesButton);
  private lazy val testButton: Button = findView(TR.testButton);

  private lazy val mainBugsenseLogoImageView: ImageView = findView(TR.mainBugsenseLogoImageView);

  private lazy val slide_layout: LinearLayout = findView(TR.main_content);

  private var multimediaType: String = null
  private var formBundle: Bundle = null

  private object Request extends Enumeration {
    val FORM = Value;
  }

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    log log "after bugsense initialization";
    setContentView(R.layout.activity_main);

    Seq(
      hitMeButton,
      wallpaperButton,
      videoButton,
      ringtoneButton,
      songButton,
      favoritesButton,
      testButton,
      mainBugsenseLogoImageView
    ) foreach (_.setOnClickListener(this));

    //SQLiteDatabase.loadLibs(this);
  }

  def onClick(view: View) {
    view.getId match {
      case R.id.hitMeButton => {
        if (formBundle == null) {
          val toast: Toast = Toast.makeText(this, R.string.empty_form_bundle, Toast.LENGTH_LONG)
          toast.show();
        } else {
          val intent: Intent = new Intent(this, classOf[MyxerSpinerActivity])
          intent.putExtras(formBundle);
          startActivity(intent);
        }
      }
      case R.id.ringtoneButton => {
        multimediaType = getResources.getStringArray(R.array.`type`)(0);
        go_to_form();
      }
      case R.id.wallpaperButton => {
        multimediaType = getResources.getStringArray(R.array.`type`)(1);
        go_to_form();
      }
      case R.id.songButton => {
        multimediaType = getResources.getStringArray(R.array.`type`)(2);
        go_to_form();
      }
      case R.id.videoButton => {
        multimediaType = getResources.getStringArray(R.array.`type`)(3);
        go_to_form();
      }
      case R.id.favoritesButton => {
        val intent = new Intent(this, classOf[FavoriteListActivity]);
        val strArray = FavoriteItem.getAllIds.map(_.toString).toArray;
        intent.putExtra(FavoriteListActivity.Extras.ID_LIST.toString, strArray);
        startActivity(intent);
      }
      case R.id.mainBugsenseLogoImageView => {
        showWebSite(BugsenseActivity.WEBSITE);
      }
      case R.id.testButton => {

        val favorites = FavoriteItem.getAll;
        var counter = 0;
        favorites foreach {
          favorite =>
            favorite.setOrdernum(counter).update;
            log log "id, ordernum => " + favorite.id.get + ", " + favorite.ordernum;
            counter += 1;
        }
        //BugSenseHandler.sendEvent("the test button was clicked");
      }
    }
  }

  private def go_to_form() {
    val intent = new Intent(MainActivity.this, classOf[FormActivity]);
    startActivityForResult(intent, Request.FORM.id);
  }

  protected override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
    super.onActivityResult(requestCode, resultCode, data)
    Request(requestCode) match {
      case Request.FORM => {
        if (resultCode == Activity.RESULT_OK) {
          formBundle = data.getExtras;
          formBundle.putString("type", multimediaType);

          log log formBundle;

          renderFormResult();
        }
      }
    }
  }

  private def renderFormResult() {
    val keySet: Set[String] = formBundle.keySet.asScala.toSet;
    slide_layout.removeViews(1, slide_layout.getChildCount - 1);
    for (cur_key <- keySet) {
      val curTextView = getLayoutInflater.inflate(R.layout.simple_text, slide_layout, false).asInstanceOf[TextView];
      curTextView.setText(
        getResources.getString(getResources.getIdentifier(cur_key, "string", getPackageName)) + ": " + formBundle.getString(cur_key)
      );
      slide_layout.addView(curTextView)
    }
  }
}


