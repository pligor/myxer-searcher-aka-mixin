package activities

import android.app.Activity
import android.view.View.OnClickListener
import com.pligor.myxer_searcher.{TR, TypedActivity, R}
import android.widget._
import android.content.Intent
import android.os.Bundle
import android.view.View
import models.MyxerModel
import preferences.{CountPreference, SafePreference, KeywordsPreference, PricingPreference}
import components.myandroid.{BugsenseActivity, MyActivity}
import components.helpers.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class FormActivity extends Activity with TypedActivity
with MyActivity
with OnClickListener
with AdMobActivity
with BugsenseActivity {
  private lazy val formKeywords: EditText = findView(TR.form_keywords);
  private lazy val formPricing: Spinner = findView(TR.form_pricing);
  private lazy val formSafe: CheckBox = findView(TR.form_safe);
  private lazy val formCount: TextView = findView(TR.form_count);

  private lazy val form_minus: Button = findView(TR.form_minus);
  private lazy val form_plus: Button = findView(TR.form_plus);
  private lazy val form_submit: Button = findView(TR.form_submit);

  private var count: Short = MyxerModel.defaultCount;
  private val inputs: Bundle = new Bundle;

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_form);

    Seq(
      form_minus,
      form_plus,
      form_submit
    ) foreach (_.setOnClickListener(this));
  }


  override def onStart() {
    super.onStart();
    formPricing.setSelection(PricingPreference.getValue);
    log log "formPricing.getSelectedItemId: " + formPricing.getSelectedItemId;
    log log "formPricing.getSelectedItemPosition: " + formPricing.getSelectedItemPosition;

    formKeywords.setText(KeywordsPreference.getValue);
    formSafe.setChecked(SafePreference.getValue);
    updateCounter(CountPreference.getValue);
  }

  override def onStop() {
    super.onStop();

    PricingPreference.setValue(formPricing.getSelectedItemId.toInt);
    KeywordsPreference.setValue(formKeywords.getText.toString);
    SafePreference.setValue(formSafe.isChecked);
    CountPreference.setValue(count);
  }

  def onClick(v: View) {
    val view_id: Int = v.getId
    if (view_id == this.form_minus.getId) {
      updateCounter((count - 1).toShort);
    }
    else if (view_id == this.form_plus.getId) {
      updateCounter((count + 1).toShort);
    }
    else if (view_id == this.form_submit.getId) {
      if (this.formKeywords.getText.length == 0) {
        Toast.makeText(FormActivity.this, R.string.no_keyword_warning, Toast.LENGTH_LONG).show();
        return;
      }
      collectFormInputs();
      setResult(Activity.RESULT_OK, (new Intent).putExtras(inputs));
      finish();
    }
  }

  private def collectFormInputs() {
    val keywords: String = this.formKeywords.getText.toString
    this.inputs.putString("search", keywords)
    val filters: Array[String] = this.getResources.getStringArray(R.array.filter)
    if (this.formSafe.isChecked) {
      this.inputs.putString("filter", filters(1))
    }
    else {
      this.inputs.putString("filter", filters(0))
    }
    val item_id: Int = this.formPricing.getSelectedItemId.asInstanceOf[Int]
    if (item_id > 0) {
      val pricings: Array[String] = this.getResources.getStringArray(R.array.pricing)
      this.inputs.putString("pricing", pricings(item_id))
    }
  }

  private def updateCounter(newCounter: Short) {
    count = newCounter;
    formCount.setText(count.toString);
    inputs.putString("count", count.toString);
  }
}