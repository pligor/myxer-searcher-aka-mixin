package activities

import com.pligor.myxer_searcher.{TR, TypedActivity, R}
import android.app.Activity
import android.view.View.OnClickListener
import java.util.{Locale, Date}
import java.text.SimpleDateFormat
import android.widget.{RatingBar, TextView, ImageView}
import models.{FavoriteItem, MyxerItemModel}
import android.os.Bundle
import android.view.View
import components.helpers.log
import components.helpers.InternetHelper._
import components.asyncbitmap.ImageDownloadTask
import android.widget.RatingBar.OnRatingBarChangeListener
import android.content.Context
import components.mydatabase.DatabaseHandler
import components.myandroid.BugsenseActivity
import components.helpers.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyxerItemActivity {
  private var item: MyxerItemModel = null;

  def setItem(newModel: MyxerItemModel)(implicit context: Context) {
    val insertId = newModel.insert;

    val finalId = if (insertId == DatabaseHandler.invalidInsertId) {
      log log "model already existed and therefore not inserted";
      newModel.id.get;
    } else {
      log log "model was inserted successfully";
      insertId;
    }

    item = MyxerItemModel.getById(finalId).get;
  }

  val formatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("el"))
}

class MyxerItemActivity extends Activity with TypedActivity with OnClickListener
with OnRatingBarChangeListener
with AdMobActivity
with BugsenseActivity {
  implicit private val context = this;

  private lazy val full_type: ImageView = findView(TR.full_type);
  private lazy val full_name: TextView = findView(TR.full_name);
  private lazy val full_artist: TextView = findView(TR.full_artist);
  private lazy val full_thumbnail: ImageView = findView(TR.full_thumbnail);
  private lazy val full_date: TextView = findView(TR.full_date);
  private lazy val full_send_button: ImageView = findView(TR.full_send_button);

  private lazy val fullRatingBar: RatingBar = findView(TR.fullRatingBar);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_myxer_item);

    if(!Option(MyxerItemActivity.item).isDefined) {
      showToast(R.string.item_is_null);
      finish();
    }

    Seq(
      full_name,
      full_artist,
      full_thumbnail,
      full_send_button
    ) foreach (_.setOnClickListener(this));

    val initialRating = MyxerItemActivity.item.getRating.getOrElse(FavoriteItem.DEFAULT_RATING);
    fullRatingBar.setRating(initialRating);

    fullRatingBar.setOnRatingBarChangeListener(this);
  }

  def onRatingChanged(ratingBar: RatingBar, rating: Float, fromUser: Boolean) {
    if(fromUser) {
      if (rating > FavoriteItem.FAVORITE_THRESHOLD) {
        MyxerItemActivity.item.makeFavorite(rating);
      } else {
        MyxerItemActivity.item.removeFromFavorites;
      }
    } else {
     log log "when rating bar is set programmatically do nothing special";
    }
  }

  override def onStart() {
    super.onStart();

    full_type.setImageResource(MyxerItemActivity.item.getDrawableByType);

    full_name.setText(MyxerItemActivity.item.getName);

    full_artist.setText("by " + MyxerItemActivity.item.getArtist);

    new ImageDownloadTask({
      bitmapOption =>
        if (bitmapOption.isDefined) {
          full_thumbnail.setImageBitmap(bitmapOption.get);
        } else {
          log log "no bitmap is downloaded";
        }
    }).execute(MyxerItemActivity.item.getBigthumbnail);

    val created: Date = MyxerItemActivity.item.getCreated;

    full_date.setText(
      getResources.getString(R.string.full_date).format(MyxerItemActivity.formatter.format(created))
    );
  }

  override def onStop() {
    super.onStop();
  }

  def onClick(view: View) {
    val link = view.getId match {
      case R.id.full_name | R.id.full_thumbnail => {
        MyxerItemActivity.item.getItemlink;
      }
      case R.id.full_artist => {
        MyxerItemActivity.item.getProfilelink;
      }
      case R.id.full_send_button => {
        MyxerItemActivity.item.getItemsendlink;
      }
    }

    showWebSite(link);
  }
}
