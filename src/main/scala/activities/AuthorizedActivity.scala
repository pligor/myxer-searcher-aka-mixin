package activities

import scala.collection.JavaConverters._
import android.accounts._
import android.app.{AlertDialog, ProgressDialog, Activity}
import android.os.Bundle
import components.helpers.log
import components.myauth.{MyAuthenticatorActivity, MyServerAuthenticator, AccountAuthenticator}
import java.util.concurrent.TimeUnit
import java.io.IOException
import android.content.{DialogInterface, Intent}
import android.app.AlertDialog.Builder
import com.pligor.myxer_searcher.R
import android.content.DialogInterface.OnClickListener
import components.WriteOnce

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AuthorizedActivity extends Activity {
  protected val authToken = new WriteOnce[String];

  private lazy val accountManager = AccountManager.get(this);

  private def showDialogAndExit(str: String) {
    val builder = new Builder(this);
    builder.setCancelable(false);

    builder.setTitle(R.string.dialog_auth_step_title);
    builder.setMessage(str);

    builder.setNeutralButton(R.string.dialog_auth_step_button,
      new OnClickListener {
        def onClick(dialog: DialogInterface, whichButton: Int) {
          assert(whichButton == DialogInterface.BUTTON_NEUTRAL);
          dialog.dismiss();
          finish();
        }
      }
    );

    builder.create().show();
  }

  private val callbackAddAccount = new AccountManagerCallback[Bundle] {
    def run(futureBundle: AccountManagerFuture[Bundle]) {
      //log log "we are inside callbackAddAccount and time is: " + System.currentTimeMillis();
      try {
        val bundle = futureBundle.getResult;
        //available keys:
        //AccountManager.KEY_ACCOUNT_NAME
        //AccountManager.KEY_ACCOUNT_TYPE
        //MyAuthenticatorActivity.OUTPUT_EXTRAS.STRING_FOR_EXIT.toString

        log log "size of extras inside my authenticator activity: " + bundle.size();

        if (bundle.isEmpty) {
          showDialogAndExit(getResources.getString(R.string.on_get_in_no_connection));
        } else {

          val accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
          val accountType = bundle.getString(AccountManager.KEY_ACCOUNT_TYPE);

          log log "accountName " + accountName;
          log log "accountType " + accountType;

          val accounts = accountManager.getAccountsByType(accountType);
          assert(accounts.length > 0, "must be larger than zero because we just added a new account");

          val accountOption = accounts.find(_.name == accountName);
          assert(accountOption.isDefined, "I insist, we just added the new account, it must be there");

          val key = MyAuthenticatorActivity.OUTPUT_EXTRAS.STRING_FOR_EXIT.toString;
          if (bundle.containsKey(key)) {
            showDialogAndExit(bundle.getString(key));
            safeDismissProgressDialog();
          } else {
            grabAuthToken(accountOption.get);
          }
        }
      } catch {
        case e: OperationCanceledException => {
          log log "OperationCanceledException";
          safeDismissProgressDialog();
          finish();
        }
        case e: AuthenticatorException => {
          log log "AuthenticatorException";
          safeDismissProgressDialog();
          finish();
        }
        case e: IOException => {
          log log "IOException";
          safeDismissProgressDialog();
          finish();
        }
      }
    }
  }

  private def grabAuthToken(account: Account) {
    log log "grabbing the auth token";
    val options = new Bundle;
    val handlerIsNullForMainThread = null;
    accountManager.getAuthToken(
      account,
      MyServerAuthenticator.defaultAuthTokenType,
      options,
      AuthorizedActivity.this,
      callbackGetAuthToken,
      handlerIsNullForMainThread
    );
  }

  private def safeDismissProgressDialog() {
    if (progressDialogOption.isDefined) {
      progressDialogOption.get.dismiss();
      progressDialogOption = None;
    }
  }

  override def onStop() {
    super.onStop();
    if (progressDialogOption.isDefined) {
      val progressDialog = progressDialogOption.get;
      if (progressDialog.isShowing) {
        progressDialog.cancel();
      }
    }
  }

  private val callbackGetAuthToken = new AccountManagerCallback[Bundle] {
    def run(futureBundle: AccountManagerFuture[Bundle]) {
      try {
        val bundle = futureBundle.getResult;
        log log "tried to get the auth token";

        if (bundle.isEmpty) {
          showDialogAndExit(getResources.getString(R.string.on_get_in_no_connection));
        } else {
          //available keys:
          //AccountManager.KEY_ACCOUNT_NAME
          //AccountManager.KEY_ACCOUNT_TYPE
          //AccountManager.KEY_AUTHTOKEN

          /*bundle.keySet().asScala foreach {key => log log "key: " + key;}*/

          val token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

          log log "auth token is: " + token;

          val key = MyAuthenticatorActivity.OUTPUT_EXTRAS.STRING_FOR_EXIT.toString;
          if (bundle.containsKey(key)) {
            showDialogAndExit(bundle.getString(key));
          } else {
            authToken setValue token;
          }
        }
      } catch {
        case e: OperationCanceledException => {
          log log "OperationCanceledException";
          finish();
        }
        case e: AuthenticatorException => {
          log log "AuthenticatorException";
          finish();
        }
        case e: IOException => {
          log log "IOException";
          finish();
        }
      } finally {
        safeDismissProgressDialog();
      }
    }
  }

  private def safeAddAccount(): Either[AccountManagerFuture[Bundle], Array[Account]] = {
    val accounts = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)

    val count = accounts.length;
    log log "addAccount with current accounts in number: " + count;
    if (count == 0) {
      Left(
        accountManager.addAccount(
          AccountAuthenticator.ACCOUNT_TYPE,
          MyServerAuthenticator.defaultAuthTokenType,
          null, //requires features
          null, //add account options
          this, //the caller activity
          callbackAddAccount,
          null //handler
        )
      );
    } else {
      Right(accounts);
    }
  }

  //private var myAsyncTask: Option[MyAsyncTask] = None;
  private var progressDialogOption: Option[ProgressDialog] = None;

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);

    val indeterminate = true;
    val cancelable = false;
    progressDialogOption = Some(ProgressDialog.show(
      this,
      "Checking in",
      "Please wait...",
      indeterminate,
      cancelable
    ));

    //this is safe because second time will not do anything
    safeAddAccount().fold(
      bundleFutureOption => {
        //do nothing because the callback will do the rest
      },
      accounts => {
        assert(accounts.length == 1, "we only allow 1 account at the present time");
        val account = accounts.head;
        grabAuthToken(account);
      }
    );
  }
}
