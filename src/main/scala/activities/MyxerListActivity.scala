package activities

import android.app.Activity
import android.view.{View, Gravity}
import com.pligor.myxer_searcher._
import android.widget.{AdapterView, Toast, ListView}
import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import components.helpers.log
import models.{MyxerItemModel, XMLParsingTask, MyxerAdapter}
import scala.collection.JavaConverters._
import components.myandroid.{MyActivity, BugsenseActivity}
import com.bugsense.trace.BugSenseHandler

object MyxerListActivity {

  object Extras extends Enumeration {
    val RAW_XML = Value;
    //val ID_LIST = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyxerListActivity extends Activity with TypedActivity with OnItemClickListener
with MyActivity
with AdMobActivity
with BugsenseActivity {
  private lazy val myxerAdapter = new MyxerAdapter;

  private lazy val itemListView: ListView = findView(TR.itemListView);
  private lazy val listItemTextView = findView(TR.listItemTextView);

  private var xmlParsingTask: Option[XMLParsingTask[_]] = None;

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    //both done from manifest
    //requestWindowFeature(Window.FEATURE_NO_TITLE)
    //getWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    setContentView(R.layout.activity_myxer_list);

    this.itemListView.setOnItemClickListener(this);

    itemListView.setAdapter(myxerAdapter);
  }


  override def onStart() {
    super.onStart();
    xmlParsingTask = Some(new XMLParsingTask[MyxerItemModel]({
      items =>
        if (items == null) {
          noItems();
        } else {
          if (items.size == 0) {
            noItems();
          } else {
            myxerAdapter.setItems(items);
          }
        }
    }));
    val xmlOption = Option(getIntent.getExtras.getString(MyxerListActivity.Extras.RAW_XML.toString));
    //if (xmlOption.isDefined) {
    assert(xmlOption.isDefined);
    xmlParsingTask.get.execute(xmlOption.get);
    listItemTextView.setText(R.string.list_title_on_search);
    /*} else {
      //val ids = getIntent.getExtras.getLongArray(MyxerListActivity.Extras.ID_LIST.toString);
      val ids = getIntent.getExtras.getStringArray(MyxerListActivity.Extras.ID_LIST.toString);
      assert(Option(ids).isDefined);
      val items = MyxerItemModel.getByIds(ids.map(_.toLong));
      myxerAdapter.setItems(new java.util.ArrayList(items.asJava));
      listItemTextView.setText(R.string.list_title_on_favorites);
    }*/
  }

  override def onStop() {
    super.onStop();
    if (xmlParsingTask.isDefined) {
      xmlParsingTask.get.cancel(true);
    }
  }

  private def noItems() {
    val toast = Toast.makeText(this, R.string.no_items_found, Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();

    val intent = new Intent(this, classOf[MainActivity]);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }


  def onItemClick(parentView: AdapterView[_], view: View, position: Int, id: Long) {
    assert(position == id);
    val itemOption = Option(myxerAdapter.getItem(position));
    if (itemOption.isDefined) {
      MyxerItemActivity setItem itemOption.get;
      startActivity(new Intent(this, classOf[MyxerItemActivity]));
    } else {
      log log "item was found null with clicked id: " + id.toInt;
      BugSenseHandler.sendEvent("on item click the item found was null. id: " + id + " and position: " + position);
    }
  }
}


