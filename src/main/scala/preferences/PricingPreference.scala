package preferences

import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object PricingPreference extends MyPreferencesCase[Long, Int] {
  val preferenceKey: String = "PricingPreference";

  val defaultValue: Long = -1L;

  def getValue(implicit context: Context): Int = getInnerValue.asInstanceOf[Long].toInt;

  protected def setVal(newValue: Int)(implicit context: Context) {
    setInnerValue(newValue);
  }
}
