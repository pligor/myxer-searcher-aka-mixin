package preferences

import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object SafePreference extends MyPreferencesCase[Boolean, Boolean] {
  val preferenceKey: String = "SafePreference";
  val defaultValue: Boolean = true;

  def getValue(implicit context: Context): Boolean = getInnerValue.asInstanceOf[Boolean];

  protected def setVal(newValue: Boolean)(implicit context: Context) {
    setInnerValue(newValue);
  }
}
