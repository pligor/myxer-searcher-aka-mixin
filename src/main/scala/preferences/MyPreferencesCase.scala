package preferences

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait MyPreferencesCase[INNER_TYPE, OUTER_TYPE] {
  //repeat the preferences' keys so that we have a more concrete reference
  val preferenceKey: String;

  val defaultValue: INNER_TYPE;

  def getValue(implicit context: Context): OUTER_TYPE;

  def isDefault(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]) = {
    (getInnerValue.asInstanceOf[INNER_TYPE] == defaultValue);
  }

  protected def setVal(newValue: OUTER_TYPE)(implicit context: Context);

  def setValue(newValue: OUTER_TYPE)
              (implicit context: Context): MyPreferencesCase[INNER_TYPE, OUTER_TYPE] = {
    setVal(newValue);
    this;
  }

  private val booleanManifest = manifest[Boolean];
  private val floatManifest = manifest[Float];
  private val intManifest = manifest[Int];
  private val longManifest = manifest[Long];
  private val stringManifest = manifest[String];

  protected def getInnerValue(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]): Any = {
    val defaultPrefs = PreferenceManager.getDefaultSharedPreferences(context);

    innerTypeManifest match {
      case `booleanManifest` => defaultPrefs.getBoolean(preferenceKey, defaultValue.asInstanceOf[Boolean]);
      case `floatManifest` => defaultPrefs.getFloat(preferenceKey, defaultValue.asInstanceOf[Float]);
      case `intManifest` => defaultPrefs.getInt(preferenceKey, defaultValue.asInstanceOf[Int]);
      case `longManifest` => defaultPrefs.getLong(preferenceKey, defaultValue.asInstanceOf[Long]);
      case `stringManifest` => defaultPrefs.getString(preferenceKey, defaultValue.asInstanceOf[String]);
    }
  }

  protected def setInnerValue(newValue: INNER_TYPE)
                             (implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]) {
    val defaultPrefsEditor1 = PreferenceManager.getDefaultSharedPreferences(context).
      edit().
      remove(preferenceKey);

    val defaultPrefsEditor2 = innerTypeManifest match {
      case `booleanManifest` => defaultPrefsEditor1.putBoolean(preferenceKey,newValue.asInstanceOf[Boolean]);
      case `floatManifest` => defaultPrefsEditor1.putFloat(preferenceKey,newValue.asInstanceOf[Float]);
      case `intManifest` => defaultPrefsEditor1.putInt(preferenceKey,newValue.asInstanceOf[Int]);
      case `longManifest` => defaultPrefsEditor1.putLong(preferenceKey,newValue.asInstanceOf[Long]);
      case `stringManifest` => defaultPrefsEditor1.putString(preferenceKey,newValue.asInstanceOf[String]);
    }
    val committed = defaultPrefsEditor2.commit();
    assert(committed);
  }
}

