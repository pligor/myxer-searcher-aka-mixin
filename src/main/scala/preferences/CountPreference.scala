package preferences

import models.MyxerModel
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object CountPreference extends MyPreferencesCase[Int, Short] {
  val preferenceKey: String = "CountPreference";
  val defaultValue: Int = MyxerModel.defaultCount;

  def getValue(implicit context: Context): Short = getInnerValue.asInstanceOf[Int].toShort;

  protected def setVal(newValue: Short)(implicit context: Context) {
    setInnerValue(newValue);
  }
}
