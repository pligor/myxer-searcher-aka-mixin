package preferences

import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object KeywordsPreference extends MyPreferencesCase[String, String] {
  //better be explicit
  val preferenceKey: String = "KeywordsPreference";
  val defaultValue: String = "";

  def getValue(implicit context: Context): String = getInnerValue.asInstanceOf[String];

  protected def setVal(newValue: String)(implicit context: Context) {
    setInnerValue(newValue);
  }
}
