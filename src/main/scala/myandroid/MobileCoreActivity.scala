package myandroid

import android.app.Activity
import android.os.Bundle
import com.ironsource.mobilcore.{ConfirmationResponse, MobileCore}
import models.MyMobileCore
import AndroidHelper._
import components.myandroid.MyActivity
import android.widget.ImageButton
import android.view.View.OnClickListener
import android.view.View
import com.ironsource.mobilcore.ConfirmationResponse.TYPE
import components.helpers.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MobileCoreActivity extends Activity with MyActivity {
  protected val alienImageButton: ImageButton;

  override def onPostCreate(savedInstanceState: Bundle) {
    super.onPostCreate(savedInstanceState);
    MobileCore.init(this, MyMobileCore.DEVELOPER_HASH, {
      if (isDebuggable) {
        MobileCore.LOG_TYPE.DEBUG;
      } else {
        MobileCore.LOG_TYPE.PRODUCTION;
      }
    });

    //MobileCore.showOfferWall(this, null);
    //MobileCore.dropIconOffer(this, null);
    alienImageButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        MobileCore.showOfferWall(activity, new ConfirmationResponse {
          def onConfirmation(responseType: TYPE) {
            responseType match {
              case ConfirmationResponse.TYPE.DECLINE => {
                log log "decline";
              }
              case ConfirmationResponse.TYPE.BACK => {
                log log "back";
              }
              case ConfirmationResponse.TYPE.AGREED => {
                log log "agreed";
              }
            }
            view.setVisibility(View.GONE);
          }
        })
      }
    });
  }
}
