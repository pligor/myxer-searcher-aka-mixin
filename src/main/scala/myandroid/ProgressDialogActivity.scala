package myandroid

import android.app.{ProgressDialog, Activity}
import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait ProgressDialogActivity extends Activity {
  protected val progressDialogs = mutable.ListBuffer.empty[ProgressDialog];

  override def onStop() {
    super.onStop();
    progressDialogs foreach {
      progressDialog =>
        if (progressDialog.isShowing) {
          progressDialog.cancel();
        }
    }
  }
}
