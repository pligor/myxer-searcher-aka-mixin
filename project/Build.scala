import sbt._

import Keys._
import AndroidKeys._

object General {
  val settings = Defaults.defaultSettings ++ Seq(
    javacOptions ++= Seq("-source", "1.6", "-target", "1.6"),
    scalacOptions ++= Seq("-deprecation", "-unchecked"),
    name := "MyxerSearcher",
    version := "1.36",
    versionCode := 11,
    scalaVersion := "2.9.2",
    platformName in Android := "android-13"
  )

  val proguardSettings = Seq(
    useProguard in Android := true,
    proguardOption in Android :=
      //recommended optimization config
      """
        |-optimizationpasses 5
        |-dontusemixedcaseclassnames
        |-dontskipnonpubliclibraryclasses
        |-dontpreverify
        |-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
      """.stripMargin +

        //mine
        """
          |-keep class scala.Function1
          |
          |-keep class android.bluetooth.BluetoothDevice {
          | *** fetchUuidsWithSdp(...);
          | *** getUuids(...);
          |}
        """.stripMargin +

        //android recommended
        """
          |-keep public class * extends android.app.Activity
          |-keep public class * extends android.app.Application
          |-keep public class * extends android.app.Service
          |-keep public class * extends android.content.BroadcastReceiver
          |-keep public class * extends android.content.ContentProvider
          |-keep public class * extends android.app.backup.BackupAgentHelper
          |-keep public class * extends android.preference.Preference
          |-keep public class com.android.vending.licensing.ILicensingService
          |-keepclasseswithmembernames class * {
          |    native <methods>;
          |}
          |-keepclasseswithmembernames class * {
          |    public <init>(android.content.Context, android.util.AttributeSet);
          |}
          |-keepclasseswithmembernames class * {
          |    public <init>(android.content.Context, android.util.AttributeSet, int);
          |}
          |-keepclassmembers enum * {
          |    public static **[] values();
          |    public static ** valueOf(java.lang.String);
          |}
          |-keep class * implements android.os.Parcelable {
          |  public static final android.os.Parcelable$Creator *;
          |}
          | """.stripMargin +

        //https://www.facebook.com/notes/hendrick-prasdito/how-to-integrating-sqlcipher-and-proguard-in-android-development/10150980582279008
        //-keep class info.guardianproject.database.** { *; }
        """
          |-keep class net.sqlcipher.**
        """.stripMargin +

        """
          |-keep public class com.bugsense.*
        """.stripMargin +

        //keep this last in order to verify by printing that all the configuration above was accepted
        """
          |-verbose
        """.stripMargin
  )

  lazy val fullAndroidSettings =
    General.settings ++ Seq(
      //resolvers += Resolver.url("fakod-releases", new URL("https://raw.github.com/FaKod/fakod-mvn-repo/master/releases"))(Resolver.ivyStylePatterns)
      //resolvers += Resolver.url("bugsense repo", new URL("http://mvn.bugsense.com"))(Resolver.ivyStylePatterns)
      //resolvers += "bugsense repo" at "http://mvn.bugsense.com"
      resolvers += "Big Bee Consultants" at "http://repo.bigbeeconsultants.co.uk/repo"
    ) ++
      AndroidProject.androidSettings ++
      TypedResources.settings ++
      proguardSettings ++
      AndroidManifestGenerator.settings ++
      AndroidMarketPublish.settings ++ Seq(
      //libraryDependencies += "org.scalatest" %% "scalatest" % "1.8" % "test",
      libraryDependencies ++= Seq(
        "org.specs2" %% "specs2" % "1.12.4.1" % "test",
        /*"commons-codec" % "commons-codec" % "1.7",
        "com.google.guava" % "guava" % "r09"*/
        "com.google.code.gson" % "gson" % "2.2.3",
        //"org.scala-libs" % "sjersey-client" % "0.2.0"
        //"com.stackmob" % "newman_2.9.2" % "0.14.2"

        //"com.bugsense.trace" % "bugsense" % "3.2",
        //"com.bugsense" % "bugsense" % "3.2.2",

        //Bee Client
        "uk.co.bigbeeconsultants" %% "bee-client" % "0.21.+",
        "org.slf4j" % "slf4j-api" % "1.7.+",
        "ch.qos.logback" % "logback-core"    % "1.0.+",
        "ch.qos.logback" % "logback-classic" % "1.0.+"
      ),

      keyalias in Android := "gpligor"
    )
}

object AndroidBuild extends Build {
  lazy val main = Project(
    "MyxerSearcher",
    file("."),
    settings = General.fullAndroidSettings
  )

  lazy val tests = Project(
    "tests",
    file("tests"),
    settings = General.settings ++
      AndroidTest.androidSettings ++
      General.proguardSettings ++ Seq(
      name := "MyxerSearcherTests"
    )
  ) dependsOn main
}
